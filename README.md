# Proverlays Server

## TODO

* Rolls/Scopes

* Fix web socket auth

* Purescipt frontend
  * Bridge stuff
  * Servant purescript possibly?
  * Ajax call to backend for stats
  * Halogen frontend
  * React components?

* Stats stuff
  * Tags? So many games can be imported instead of just ECL.
  