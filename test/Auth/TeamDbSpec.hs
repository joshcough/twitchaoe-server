
module Auth.TeamDbSpec (spec) where

import           Data.Aeson                  (object)
import           Test.Hspec

import           Auth.Models
import           Auth.TeamAPI
import           Auth.TeamStorage            (getTeamByTeamname)
import           Config                      (AppT, Config)
import           Database.Persist.Postgresql (toSqlKey)
import           Helpers

spec :: Spec
spec = before_ runServer . around setupTeardown $
    describe "Team" $ do
        it "getTeamById fetches Team" $ \config -> do
            (Team _ name settings) <- withTeam config return
            name     `shouldBe` testTeam1Name
            settings `shouldBe` testUser1Settings

        it "deleteTeam deletes team" $ \config -> do
            mTeam <- withTeam config $ \t -> do
                deleteTeam adminUser (toSqlKey . teamId $ t)
                getTeamByTeamname testTeam1Name
            mTeam `shouldBe` Nothing

        it "updateTeam updates team" $ \config -> do
            let ussr      = "USSR"
            let emptySett = object []
            u' <- withTeam config $ \(Team tid _ _) -> do
                updateTeam  adminUser (toSqlKey tid) $ Team tid ussr emptySett
                getTeamById adminUser (toSqlKey tid)
            teamName  u' `shouldBe` ussr

withTeam :: Config -> (Team -> AppT IO a) -> IO a
withTeam config f = runAppToIO config $ do
    tid  <- createTeam adminUser createTeam1Obj
    team <- getTeamById adminUser tid
    f team
