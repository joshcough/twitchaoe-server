
module Auth.TeamServerWaiSpec (spec) where

import           Data.Aeson                  (Value(..))
import           Test.Hspec
import           Test.Hspec.Wai
import           Test.Hspec.Wai.JSON

import           Helpers

spec :: Spec
spec = with cleanDbAndStartApp $ do
    describe "GET /teams/1" $
        it "responds with 404" $ withAdminAuth $ \u ->
            get' "/teams/1" u `shouldRespondWith` 404

    describe "POST /teams followed by GET /teams/1" $
        it "responds with a team id" $ withAdminAuth $ \u -> do
            postJson' "/teams" u createTeam1V `shouldRespondWith` [json|1|]
            get' "/teams/1" u `shouldRespondWith` [json|{"teamName":"USA","teamId":1,"teamSettings":{}}|]

    describe "POST /teams followed by PUT /teams/1" $
        it "responds with a team id" $ withAdminAuth $ \u -> do
            postJson' "/teams"   u createTeam1V `shouldRespondWith` [json|1|]
            putJson'  "/teams/1" u testTeam1V    `shouldRespondWith` 200

    describe "POST followed by DELETE followed by GET" $
        it "responds with 404" $ withAdminAuth $ \u -> do
            postJson' "/teams"   u createTeam1V `shouldRespondWith` [json|1|]
            delete'   "/teams/1" u             `shouldRespondWith` 200
            get'      "/teams/1" u             `shouldRespondWith` 404

    describe "Update team settings json" $
        it "updates" $ withAdminAuth $ \u -> do
            postJson' "/teams" u createTeam1V `shouldRespondWith` [json|1|]
            postJson' "/teams/1/settings/x" u (Number 6) `shouldRespondWith` 200
            get' "/teams/1" u `shouldRespondWith` [json|{"teamName":"USA","teamId":1,"teamSettings":{"x":6}}|]
            get' "/teams/1/settings/x" u `shouldRespondWith` [json|6|]
