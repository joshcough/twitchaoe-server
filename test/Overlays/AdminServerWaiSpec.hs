
module Overlays.AdminServerWaiSpec (spec) where

-- import           Data.Aeson                           (Value(..), ToJSON(..), object)
import           Test.Hspec
{-import           Test.Hspec.Wai
import           Test.Hspec.Wai.JSON

import           Auth.Models
import           Helpers
import           Overlays.SceneServerWaiSpec          (createScene1V)

createTestUser1Obj2 :: CreateUser
createTestUser1Obj2 = CreateUser "maaan" 1 "maaan@cmon.com" "password" (object [])

createUser2V :: Value
createUser2V = toJSON createTestUser1Obj2-}

spec :: Spec
spec = return ()

{-
before_ setupTeardownDb $ with testApp $
    describe "clone a user" $
        it "clones a user" $ withAdminAuth $ \a -> withUserAuth $ \u -> do
            -- create initial user
            postJson' "/scenes"       u createScene1V      `shouldRespondWith` [json|{"createSceneResponseSceneItemIds":[1,2],"createSceneResponseSceneId":1}|]
            -- clone the user
            postJson' "/admin/users/1/clone" a createUser2V `shouldRespondWith` 200
--TODO
            -- test that all data was cloned.
--             get' "/users/2"           u `shouldRespondWith` [json|{"userName":"maaan","userId":2,"userEmail":"maaan@cmon.com", "userIsAdmin":false}|]
--             get' "/scenes/2"          u `shouldRespondWith` [json|{"sceneSettings":5,"sceneUserId":1,"sceneDimensions":{"height":6,"width":5},"sceneItems":[{"sceneItemBody":8,"sceneItemId":1},{"sceneItemBody":9,"sceneItemId":2}],"sceneId":1,"sceneName":"scene1"}|]
-}