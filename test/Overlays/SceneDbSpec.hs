
module Overlays.SceneDbSpec (spec) where

-- import           Control.Arrow               ((&&&))
import           Test.Hspec
{-import           Data.Aeson                  (Value, decode)
import           Data.Foldable               (traverse_)
import           Data.Maybe                  (fromJust)
import           Data.Text                   (Text)

import           Auth.Models
import           Auth.UserDbSpec             (withUser)
import           Config                      (AppT, runDb)-}
import           Helpers                     (setupTeardown)
{-import           Overlays.Api.Scene
import           Overlays.Storage.Scene.SceneStorage (getDbSceneBySceneId)
import           Overlays.Models-}

{-
scene1 :: Text
scene1 = "Scene1"

item1, item2, item3, settings :: Value
item1    = fromJust $ decode "{\"foo\": 123}"
item2    = fromJust $ decode "{\"goo\": 734}"
item3    = fromJust $ decode "{\"hoo\": 875}"
settings = fromJust $ decode "{\"zoo\": 123}"
-}

spec :: Spec
spec = around setupTeardown $ do
    return ()
{-
    describe "Scene" $ do
        it "getScenesByUserId fetches Scene" $ \config -> do
            dbScenes <- withUser config $ \u -> do
                createScene u $ CreateScene (userId u) scene1 settings []
                getScenesByUserId (userId u)
            _ dbScenes

        it "getScenesBySceneId fetches Scene" $ \config -> do
            (dbName, dbSett) <- withUser config $ \u -> do
                (CreateSceneResponse sid _) <- createScene u $ CreateScene (userId u) scene1 settings []
                (sceneName &&& sceneSettings) <$> getSceneBySceneId u sid
            dbName `shouldBe` scene1
            dbSett `shouldBe` settings

        it "deleteScene deletes scene (Kappa)" $ \config -> do
            mScene <- withUser config $ \u -> do
                (CreateSceneResponse sid _) <- createScene u $ CreateScene (userId u) scene1 settings []
                deleteScene u sid
                runDb $ getDbSceneBySceneId u sid
            mScene  `shouldBe` Nothing

    describe "Scene Items" $ do
        it "getScenesByUserId fetches all the SceneItems" $ \config -> do
            dbScenesItems <- withUser config $ \u -> do
                _ <- createScene u $ CreateScene (userId u) scene1 settings [item1, item2, item3]
                getSceneItemBodies $ getScenesByUserId u (userId u)
            dbScenesItems `shouldBe` [[item1, item2, item3]]

        it "deleteSceneItem deletes scene item (Kappa)" $ \config -> do
            dbScenesItems <- withUser config $ \u -> do
                let cs = CreateScene (userId u) scene1 settings [item1, item2, item3]
                (CreateSceneResponse _ siids) <- createScene u cs
                traverse_ (deleteSceneItem u) siids
                getSceneItemBodies $ getScenesByUserId u (userId u)
            dbScenesItems  `shouldBe` [[]]

getSceneItemBodies :: AppT IO [Scene] -> AppT IO [[Value]]
getSceneItemBodies = fmap (fmap (fmap sceneItemBody . sceneItems))
-}