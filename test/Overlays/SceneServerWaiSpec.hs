
module Overlays.SceneServerWaiSpec (spec) where

-- import           Data.Aeson                  (Value(..), ToJSON(..))
import           Test.Hspec
{-
import           Test.Hspec.Wai
import           Test.Hspec.Wai.JSON

import           Auth.Models
import           Helpers
import           Overlays.Models
--}

{-
createScene1 :: CreateScene
createScene1 = CreateScene "scene1" (Number 5) [Number 8, Number 9]

createScene1V :: Value
createScene1V = toJSON createScene1

-- i'm really using this for updating the scene... TODO: consider creating an UpdateScene model
createScene2 :: CreateScene
createScene2 = CreateScene "scene2" (Number 6) [Number 8, Number 9]

createScene2V :: Value
createScene2V = toJSON createScene2
-}

spec :: Spec
spec = return ()

{-before_ setupTeardownDb $ with testApp $i
    do
    describe "GET /scenes/1" $
        it "responds with 404" $ withAdminAuth $ \u ->
            get' "/scenes/1" u `shouldRespondWith` 404

    describe "POST /scenes followed by GET /scenes/1" $
        it "responds with a scene id" $ withUserAuth $ \u -> do
            postJson' "/scenes" u createScene1V `shouldRespondWith` [json|{"createSceneResponseSceneItemIds":[1,2],"createSceneResponseSceneId":1}|]
            get' "/scenes/1"    u `shouldRespondWith` [json|{"sceneSettings":5,"sceneUserId":1,"sceneDimensions":{"height":6,"width":5},"sceneItems":[{"sceneItemBody":8,"sceneItemId":1},{"sceneItemBody":9,"sceneItemId":2}],"sceneId":1,"sceneName":"scene1"}|]

    describe "POST /scenes followed by PUT /scenes/1" $
        it "responds with a user id" $ withUserAuth $ \u -> do
            postJson' "/scenes"   u createScene1V `shouldRespondWith` [json|{"createSceneResponseSceneItemIds":[1,2],"createSceneResponseSceneId":1}|]
            putJson'  "/scenes/1" u createScene2V `shouldRespondWith` [json|{"createSceneResponseSceneItemIds":[3,4],"createSceneResponseSceneId":1}|]

    describe "POST /scenes followed by DELETE followed by GET" $
        it "creates, and then deletes scene" $ withUserAuth $ \u -> do
            postJson' "/scenes"   u createScene1V `shouldRespondWith` [json|{"createSceneResponseSceneItemIds":[1,2],"createSceneResponseSceneId":1}|]
            delete'   "/scenes/1" u               `shouldRespondWith` 200
            get'      "/scenes/1" u               `shouldRespondWith` 404

    describe "POST /scenes followed by GET /users/1/scenes" $
        it "responds with a scene" $ withUserAuth $ \u -> do
            postJson' "/scenes" u createScene1V `shouldRespondWith` [json|{"createSceneResponseSceneItemIds":[1,2],"createSceneResponseSceneId":1}|]
            get' "/users/1/scenes" u `shouldRespondWith` [json|[{"sceneSettings":5,"sceneUserId":1,"sceneDimensions":{"height":6,"width":5},"sceneItems":[{"sceneItemBody":8,"sceneItemId":1},{"sceneItemBody":9,"sceneItemId":2}],"sceneId":1,"sceneName":"scene1"}]|]

    describe "POST /scenes followed by GET /teams/1/scenes" $
        it "responds with a scene" $ withAdminAuth $ \a -> withUserAuth $ \u -> do
            postJson' "/users"  a createTestUser2V   `shouldRespondWith` [json|2|]
            postJson' "/scenes" u createScene1V `shouldRespondWith` [json|{"createSceneResponseSceneItemIds":[1,2],"createSceneResponseSceneId":1}|]
            postJson' "/scenes" a createScene2V `shouldRespondWith` [json|{"createSceneResponseSceneItemIds":[3,4],"createSceneResponseSceneId":2}|]
            get' "/teams/1/scenes" u `shouldRespondWith` [json|[{"sceneSettings":5,"sceneUserId":1,"sceneDimensions":{"height":6,"width":5},"sceneItems":[{"sceneItemBody":8,"sceneItemId":1},{"sceneItemBody":9,"sceneItemId":2}],"sceneId":1,"sceneName":"scene1"},{"sceneSettings":6,"sceneUserId":1,"sceneDimensions":{"height":7,"width":6},"sceneItems":[{"sceneItemBody":8,"sceneItemId":3},{"sceneItemBody":9,"sceneItemId":4}],"sceneId":2,"sceneName":"scene2"}]|]

    describe "post scene, then post scene item, then get scene" $
        it "responds with a scene" $ withUserAuth $ \u -> do
            postJson' "/scenes" u createScene1V `shouldRespondWith` [json|{"createSceneResponseSceneItemIds":[1,2],"createSceneResponseSceneId":1}|]
            postJson' "/scenes/items/1" u (Number 10) `shouldRespondWith` [json|3|]
            get' "/scenes/1"   u `shouldRespondWith` [json|{"sceneSettings":5,"sceneUserId":1,"sceneDimensions":{"height":6,"width":5},"sceneItems":[{"sceneItemBody":8,"sceneItemId":1},{"sceneItemBody":9,"sceneItemId":2}, {"sceneItemBody":10,"sceneItemId":3}],"sceneId":1,"sceneName":"scene1"}|]

    describe "post scene, then post scene item, then delete scene item" $
        it "responds with a scene" $ withUserAuth $ \u -> do
            postJson' "/scenes" u createScene1V `shouldRespondWith` [json|{"createSceneResponseSceneItemIds":[1,2],"createSceneResponseSceneId":1}|]
            -- TODO: the URL patterns here are really weird. one uses sceneId, but the next is sceneItemId
            postJson' "/scenes/items/1" u (Number 10) `shouldRespondWith` [json|3|]
            delete'   "/scenes/items/3" u  `shouldRespondWith` 200
            get'      "/users/1/scenes" u `shouldRespondWith` [json|[{"sceneSettings":5,"sceneUserId":1,"sceneDimensions":{"height":6,"width":5},"sceneItems":[{"sceneItemBody":8,"sceneItemId":1},{"sceneItemBody":9,"sceneItemId":2}],"sceneId":1,"sceneName":"scene1"}]|]
-}