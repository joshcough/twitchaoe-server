
module Overlays.WebsocketServerWaiSpec (spec) where

import           Data.Aeson                  (ToJSON(..), Value(..))
import           Test.Hspec
import           Test.Hspec.Wai

import           Helpers
import           Overlays.Models                      (WebsocketMessage(..))

spec :: Spec
spec = with cleanDbAndStartApp $
    describe "login" $
        it "sets set-cookie header" . withUserAuth $ \user -> do
            let msg = toJSON $ WebsocketMessage "message-type" 1 1 (Number 0) -- just a generic message with any content
            postJson' "/stream/send" user msg `shouldRespondWith` 200
