module Stats (
    runPlayerStatsReq
  , renderPlayersResponse
  ) where

import Prelude

import Affjax as AX
import Affjax.ResponseFormat as ResponseFormat
import Data.Argonaut (class DecodeJson, decodeJson) as J
import Data.Argonaut.Core (Json) as J
import Data.Array (length, mapMaybe)
import Data.Either (Either(..))
import Data.HTTP.Method (Method(..))
import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Aff (Aff, launchAff_)
import Effect.Class.Console (log)

import Halogen as H
import Halogen.HTML (HTML, div_, h2_, p_, style_, table_, text, td_, th_, tr_)

import Stats.Aggregations (AggregatedPlayerStats(..), AggregatedStats(..), AggregatedStatsGroup(..))
import Stats.Models (Player(..))
import Stats.Html (AggregatedPlayerStatsResponse(..))

main :: Effect Unit
main = launchAff_ $ do
  res <- runPlayerStatsReq
  case res of
    Left err -> log $ "GET /stats/players response failed to decode: " <> err
    Right (AggregatedPlayerStatsResponse stats) -> log $ "GET /stats/players response: " <> show (length stats)

playerStatsReq :: AX.Request J.Json
playerStatsReq = jsonGet "/stats/players"

runPlayerStatsReq :: Aff (Either String AggregatedPlayerStatsResponse)
runPlayerStatsReq = runReq playerStatsReq

runReq :: forall a. J.DecodeJson a => AX.Request J.Json -> Aff (Either String a)
runReq req = do
  res <- AX.request req
  pure $ case res.body of
    Left err -> Left $ ResponseFormat.printResponseFormatError err
    Right json -> J.decodeJson json

jsonGet :: String -> AX.Request J.Json
jsonGet path = AX.defaultRequest {
    url = path
  , method = Left GET
  -- import Affjax.RequestHeader (RequestHeader(..))
  -- import Data.MediaType.Common (applicationJSON)
  --, headers = [ContentType applicationJSON]
  , responseFormat = ResponseFormat.json }


renderPlayersResponse :: forall a b. AggregatedPlayerStatsResponse -> H.HTML a b
renderPlayersResponse (AggregatedPlayerStatsResponse []) = div_ [ p_ [ text "No stats found" ] ]
renderPlayersResponse (AggregatedPlayerStatsResponse aggs) = div_ [
    h2_ [ text "Players" ]
  , div_ [
      f "Totals"     $ mapMaybe (\(AggregatedPlayerStats r) -> (\(AggregatedStatsGroup p) -> p.aggStatsTotals) r.playerStatsAgg) aggs
    , f "1v1s"       $ mapMaybe (\(AggregatedPlayerStats r) -> (\(AggregatedStatsGroup p) -> p.aggStats1v1s) r.playerStatsAgg) aggs
    , f "Team Games" $ mapMaybe (\(AggregatedPlayerStats r) -> (\(AggregatedStatsGroup p) -> p.aggStatsTgs) r.playerStatsAgg) aggs
    ]
  ]
  where
  f header' aggs' = div_ [ table_ $ [headersRow] <> (aggregatedStatsHtml <$> aggs') ]

aggregatedStatsHtml :: AggregatedStats -> forall p i. HTML p i
aggregatedStatsHtml (AggregatedStats s) = tr_ [
    statsRowRight "daut"
  , statsRowRight s.statsNumWins
  , statsRowRight s.statsNumLosses
  , statsRowRight $ rounded s.statsWinPct
  , statsRowRight s.statsNumMVPs
  , statsRowRight $ rounded s.statsKDRatio
--   , aggregatedCommonStatsHtml statsCommonStats
  ]

headersRow :: forall p i. HTML p i
headersRow = th_ $ statsHeaders <> commonHeaders

statsHeaders :: forall p i. Array (HTML p i)
statsHeaders = [
    th_ [ text "Player" ]
  , th_ [ text "Wins" ]
  , th_ [ text "Losses" ]
  , th_ [ text "Win Pct." ]
  , th_ [ text "MVPs" ]
  , th_ [ text "K/D Ratio" ]
  ]

commonHeaders :: forall p i. Array (HTML p i)
commonHeaders = [
    th_ [ text "Games Played" ]
  , th_ [ text "Avg HP Damage" ]
  , th_ [ text "Avg Units Killed" ]
  , th_ [ text "Avg Food Collected" ]
  , th_ [ text "Avg Wood Collected" ]
  , th_ [ text "Avg Gold Collected" ]
  , th_ [ text "Avg Stone Collected" ]
  , th_ [ text "Avg Tribute Sent" ]
  , th_ [ text "Avg Tribute Received" ]
  , th_ [ text "Avg Map Exploration" ]
  , th_ [ text "Avg Relic Gold" ]
  , th_ [ text "Avg Game Length" ]
  , th_ [ text "Avg Feudal Time" ]
  , th_ [ text "Avg Castle Time" ]
  , th_ [ text "Avg Imperial Time" ]
  ]

statsRowRight :: forall p i a. Show a => a -> HTML p i
statsRowRight statVal = td_ [style_ [text "text-align:right"], text $ show statVal]

statsRowLeft :: forall p i a. Show a => a -> HTML p i
statsRowLeft statVal = td_ [style_ [text "text-align:left"], text $ show statVal]

-- rounded :: PrintfArg t => t -> Html
-- rounded d = toHtml (printf "%.2f" d :: String)
rounded = identity

{-
aggregatedStatsHtml :: AggregatedStats -> Html
aggregatedStatsHtml AggregatedStats {..} = do
    statsRowRight statsNumWins
    statsRowRight statsNumLosses
    statsRowRight $ rounded statsWinPct
    statsRowRight statsNumMVPs
    statsRowRight $ rounded statsKDRatio
    aggregatedCommonStatsHtml statsCommonStats

aggregatedMapStatsHtml :: AggregatedMapStats -> Html
aggregatedMapStatsHtml AggregatedMapStats {..} = do
    statsRowRight $ mapName mapStatsMap
    aggregatedCommonStatsHtml mapStatsCommonStats

aggregatedCommonStatsHtml :: AggregatedCommonStats -> Html
aggregatedCommonStatsHtml AggregatedCommonStats {..} = do
    statsRowRight   statsGamesPlayed
    statsRowRight $ truncated statsHPDamage
    statsRowRight $ truncated statsNumKilled
    statsRowRight $ truncated statsAvgFood
    statsRowRight $ truncated statsAvgWood
    statsRowRight $ truncated statsAvgGold
    statsRowRight $ truncated statsAvgStone
    statsRowRight $ truncated statsTributeSent
    statsRowRight $ truncated statsTributeReceived
    statsRowRight $ rounded   statsAvgMapExp
    statsRowRight $ truncated statsRelicGold
    statsRowRight $ timeHtml  statsAvgGameLength
    uptimesHtml statsAvgUptimes


uptimesHtml :: Uptimes -> Html
uptimesHtml Uptimes{..} = do
    statsRowRight $ uptimeMaybe uptimeFeudal
    statsRowRight $ uptimeMaybe uptimeCastle
    statsRowRight $ uptimeMaybe uptimeImperial
    where
    uptimeMaybe Nothing  = "-"
    uptimeMaybe (Just u) = timeHtml u

timeHtml :: Time -> Html
timeHtml Time{..} = do
    when (hours > 0) $ toHtml hours >> sep
    pad minutes
    sep
    pad seconds
    where
    pad t = toHtml $ (if t < 10 then "0" else "") ++ show t
    sep = toHtml (":" :: String)

rounded :: PrintfArg t => t -> Html
rounded d = toHtml (printf "%.2f" d :: String)

truncated :: RealFrac a => a -> Html
truncated d = toHtml $ show (floor d :: Int)

instance ToMarkup [Game] where
    toMarkup [] = div $ p "No games found."
    toMarkup xs = forM_ xs gameHtml

gameHtml :: Game -> Html
gameHtml g@Game{..} = div $ do
    div . table $ do
        tr gameDataHeaders
        tr $ gameDataHtml g
    div . table $ do
        tr playerHeaders
        forM_  gamePlayerStats (tr . playerStatsHtml)
    br

gameDataHeaders :: Markup
gameDataHeaders = do
    th "Id"
    th "Name"
    th "Map"

playerHeaders :: Markup
playerHeaders = do
    th "Player"
    th "Team"
    th "Civ"
    th "Win"

gameDataHtml :: Game -> Html
gameDataHtml Game {..} = do
    statsRowRight $ gameDataId gameGameData
    statsRowLeft . gameName $ gameDataBody gameGameData
    statsRowLeft $ mapName gameDataGameMap
    statsRowRight . timeHtml . game_length $ gameDataBody gameGameData

playerStatsHtml :: (Player, Civilization, PlayerStats) -> Html
playerStatsHtml (player, civ, PlayerStats {..}) = do
    statsRowLeft $ playerName player
    statsRowLeft $ maybe "-" teamName $ playerTeam player
    statsRowLeft $ civName civ
    statsRowLeft $ p $ if victory then "Yes" else "No"
-}
