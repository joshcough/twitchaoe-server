-- File auto generated by purescript-bridge! --
module Stats.Models where

import Data.Lens (Iso', Lens', Prism', lens, prism')
import Data.Lens.Iso.Newtype (_Newtype)
import Data.Lens.Record (prop)
import Data.Maybe (Maybe, Maybe(..))
import Data.Newtype (class Newtype)
import Data.Symbol (SProxy(SProxy))
import GHC.Int (Int64)
import Prim (Array, Boolean, Int, String)
import Prelude
import Data.Generic.Rep (class Generic)
import Data.Argonaut (class DecodeJson, class EncodeJson)
import Data.Argonaut.Generic (genericDecodeJson, genericEncodeJson)

newtype GameData =
    GameData {
      gameDataId :: Int64
    , gameDataBody :: GameDataBody
    }

derive instance eqGameData :: Eq GameData
derive instance genericGameData :: Generic GameData _
derive instance newtypeGameData :: Newtype GameData _

instance decodeGameData :: DecodeJson GameData where
    decodeJson = genericDecodeJson
instance encodeGameData :: EncodeJson GameData where
    encodeJson = genericEncodeJson

--------------------------------------------------------------------------------
_GameData :: Iso' GameData { gameDataId :: Int64, gameDataBody :: GameDataBody}
_GameData = _Newtype

--------------------------------------------------------------------------------
newtype GameDataBody =
    GameDataBody {
      gameName :: String
    , scenario_filename :: String
    , num_players :: Int
    , duration :: Int
    , allow_cheats :: Boolean
    , complete :: Int
    , map_size :: Int
    , map_id :: Int
    , population :: Int
    , victory_type :: Int
    , starting_age :: Int
    , resources :: Int
    , all_techs :: Boolean
    , team_together :: Boolean
    , reveal_map :: Int
    , is_death_match :: Boolean
    , is_regicide :: Boolean
    , lock_teams :: Boolean
    , lock_speed :: Boolean
    , game_length :: Time
    }

derive instance eqGameDataBody :: Eq GameDataBody
derive instance genericGameDataBody :: Generic GameDataBody _
derive instance newtypeGameDataBody :: Newtype GameDataBody _

instance decodeGameDataBody :: DecodeJson GameDataBody where
    decodeJson = genericDecodeJson
instance encodeGameDataBody :: EncodeJson GameDataBody where
    encodeJson = genericEncodeJson

--------------------------------------------------------------------------------
_GameDataBody :: Iso' GameDataBody { gameName :: String, scenario_filename :: String, num_players :: Int, duration :: Int, allow_cheats :: Boolean, complete :: Int, map_size :: Int, map_id :: Int, population :: Int, victory_type :: Int, starting_age :: Int, resources :: Int, all_techs :: Boolean, team_together :: Boolean, reveal_map :: Int, is_death_match :: Boolean, is_regicide :: Boolean, lock_teams :: Boolean, lock_speed :: Boolean, game_length :: Time}
_GameDataBody = _Newtype

--------------------------------------------------------------------------------
newtype GameImportResult =
    GameImportResult {
      gameData :: GameDataBody
    , gameMap :: Maybe String
    , playerStats :: Array PlayerStats
    }

derive instance eqGameImportResult :: Eq GameImportResult
derive instance genericGameImportResult :: Generic GameImportResult _
derive instance newtypeGameImportResult :: Newtype GameImportResult _

instance decodeGameImportResult :: DecodeJson GameImportResult where
    decodeJson = genericDecodeJson
instance encodeGameImportResult :: EncodeJson GameImportResult where
    encodeJson = genericEncodeJson

--------------------------------------------------------------------------------
_GameImportResult :: Iso' GameImportResult { gameData :: GameDataBody, gameMap :: Maybe String, playerStats :: Array PlayerStats}
_GameImportResult = _Newtype

--------------------------------------------------------------------------------
newtype Player =
    Player {
      playerId :: Int64
    , playerName :: String
    , playerTeam :: Maybe AoeTeam
    }

derive instance eqPlayer :: Eq Player
derive instance ordPlayer :: Ord Player
derive instance genericPlayer :: Generic Player _
derive instance newtypePlayer :: Newtype Player _

instance decodePlayer :: DecodeJson Player where
    decodeJson = genericDecodeJson
instance encodePlayer :: EncodeJson Player where
    encodeJson = genericEncodeJson

--------------------------------------------------------------------------------
_Player :: Iso' Player { playerId :: Int64, playerName :: String, playerTeam :: Maybe AoeTeam}
_Player = _Newtype

--------------------------------------------------------------------------------
newtype PlayerStats =
    PlayerStats {
      playerStatsPlayerName :: String
    , score :: Int
    , victory :: Boolean
    , civilization :: Civilization
    , color :: Int
    , team :: Int
    , allies_count :: Int
    , mvp :: Boolean
    , result :: Int
    , military_score :: Int
    , units_killed :: Int
    , hp_killed :: Int
    , units_lost :: Int
    , buildings_razed :: Int
    , hp_razed :: Int
    , buildings_lost :: Int
    , units_converted :: Int
    , player_units_killed :: Array Int
    , player_buildings_razed :: Array Int
    , economy_score :: Int
    , food_collected :: Int
    , wood_collected :: Int
    , stone_collected :: Int
    , gold_collected :: Int
    , tribute_sent :: Int
    , tribute_received :: Int
    , trade_profit :: Int
    , relic_gold :: Int
    , player_tribute_sent :: Array Int
    , tech_score :: Int
    , feudal_time :: Int
    , castle_time :: Int
    , imperial_time :: Int
    , map_exploration :: Int
    , research_count :: Int
    , research_percent :: Int
    , society_score :: Int
    , total_wonders :: Int
    , total_castles :: Int
    , relics_captured :: Int
    , villager_high :: Int
    }

derive instance eqPlayerStats :: Eq PlayerStats
derive instance genericPlayerStats :: Generic PlayerStats _
derive instance newtypePlayerStats :: Newtype PlayerStats _

instance decodePlayerStats :: DecodeJson PlayerStats where
    decodeJson = genericDecodeJson
instance encodePlayerStats :: EncodeJson PlayerStats where
    encodeJson = genericEncodeJson

--------------------------------------------------------------------------------
_PlayerStats :: Iso' PlayerStats { playerStatsPlayerName :: String, score :: Int, victory :: Boolean, civilization :: Civilization, color :: Int, team :: Int, allies_count :: Int, mvp :: Boolean, result :: Int, military_score :: Int, units_killed :: Int, hp_killed :: Int, units_lost :: Int, buildings_razed :: Int, hp_razed :: Int, buildings_lost :: Int, units_converted :: Int, player_units_killed :: Array Int, player_buildings_razed :: Array Int, economy_score :: Int, food_collected :: Int, wood_collected :: Int, stone_collected :: Int, gold_collected :: Int, tribute_sent :: Int, tribute_received :: Int, trade_profit :: Int, relic_gold :: Int, player_tribute_sent :: Array Int, tech_score :: Int, feudal_time :: Int, castle_time :: Int, imperial_time :: Int, map_exploration :: Int, research_count :: Int, research_percent :: Int, society_score :: Int, total_wonders :: Int, total_castles :: Int, relics_captured :: Int, villager_high :: Int}
_PlayerStats = _Newtype

--------------------------------------------------------------------------------
newtype AoeTeam =
    AoeTeam {
      teamId :: Int64
    , teamName :: String
    }

derive instance eqAoeTeam :: Eq AoeTeam
derive instance ordAoeTeam :: Ord AoeTeam
derive instance genericAoeTeam :: Generic AoeTeam _
derive instance newtypeAoeTeam :: Newtype AoeTeam _

instance decodeAoeTeam :: DecodeJson AoeTeam where
    decodeJson = genericDecodeJson
instance encodeAoeTeam :: EncodeJson AoeTeam where
    encodeJson = genericEncodeJson

--------------------------------------------------------------------------------
_AoeTeam :: Iso' AoeTeam { teamId :: Int64, teamName :: String}
_AoeTeam = _Newtype

--------------------------------------------------------------------------------
newtype GameMap =
    GameMap {
      mapId :: Int64
    , mapName :: String
    }

derive instance eqGameMap :: Eq GameMap
derive instance ordGameMap :: Ord GameMap
derive instance genericGameMap :: Generic GameMap _
derive instance newtypeGameMap :: Newtype GameMap _

instance decodeGameMap :: DecodeJson GameMap where
    decodeJson = genericDecodeJson
instance encodeGameMap :: EncodeJson GameMap where
    encodeJson = genericEncodeJson

--------------------------------------------------------------------------------
_GameMap :: Iso' GameMap { mapId :: Int64, mapName :: String}
_GameMap = _Newtype

--------------------------------------------------------------------------------
newtype Civilization =
    Civilization {
      civId :: Int64
    , civName :: String
    }

derive instance eqCivilization :: Eq Civilization
derive instance ordCivilization :: Ord Civilization
derive instance genericCivilization :: Generic Civilization _
derive instance newtypeCivilization :: Newtype Civilization _

instance decodeCivilization :: DecodeJson Civilization where
    decodeJson = genericDecodeJson
instance encodeCivilization :: EncodeJson Civilization where
    encodeJson = genericEncodeJson

--------------------------------------------------------------------------------
_Civilization :: Iso' Civilization { civId :: Int64, civName :: String}
_Civilization = _Newtype

--------------------------------------------------------------------------------
newtype Time =
    Time {
      hours :: Int
    , minutes :: Int
    , seconds :: Int
    }

derive instance eqTime :: Eq Time
derive instance ordTime :: Ord Time
derive instance genericTime :: Generic Time _
derive instance newtypeTime :: Newtype Time _

instance decodeTime :: DecodeJson Time where
    decodeJson = genericDecodeJson
instance encodeTime :: EncodeJson Time where
    encodeJson = genericEncodeJson

--------------------------------------------------------------------------------
_Time :: Iso' Time { hours :: Int, minutes :: Int, seconds :: Int}
_Time = _Newtype

--------------------------------------------------------------------------------
newtype Uptimes =
    Uptimes {
      uptimeFeudal :: Maybe Time
    , uptimeCastle :: Maybe Time
    , uptimeImperial :: Maybe Time
    }

derive instance eqUptimes :: Eq Uptimes
derive instance ordUptimes :: Ord Uptimes
derive instance genericUptimes :: Generic Uptimes _
derive instance newtypeUptimes :: Newtype Uptimes _

instance decodeUptimes :: DecodeJson Uptimes where
    decodeJson = genericDecodeJson
instance encodeUptimes :: EncodeJson Uptimes where
    encodeJson = genericEncodeJson

--------------------------------------------------------------------------------
_Uptimes :: Iso' Uptimes { uptimeFeudal :: Maybe Time, uptimeCastle :: Maybe Time, uptimeImperial :: Maybe Time}
_Uptimes = _Newtype

--------------------------------------------------------------------------------
