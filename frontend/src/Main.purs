module Main where

import Prelude
-- import Control.Coroutine as CR
import Data.Maybe (Maybe(..), maybe)
import Effect (Effect)
import Data.Either (Either(..))
import Effect.Class.Console (log)
import Halogen as H
import Halogen.Aff as HA
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.VDom.Driver (runUI)

import Button as Button
import Stats as Stats
import Stats.Html (AggregatedPlayerStatsResponse)

main :: Effect Unit
main = do
  HA.runHalogenAff do
    stats <- Stats.runPlayerStatsReq
    case stats of
      Left err -> log $ "error with stats: " <> err
      Right stats' -> do
        io <- HA.awaitBody >>= runUI (component stats') unit
        pure unit

data Query a
  = HandleButton Button.Message a
  | CheckButtonState a

type State =
  { toggleCount :: Int
  , buttonState :: Maybe Boolean
  }

data Slot = ButtonSlot
derive instance eqButtonSlot :: Eq Slot
derive instance ordButtonSlot :: Ord Slot

component :: forall m. AggregatedPlayerStatsResponse -> H.Component HH.HTML Query Unit Void m
component stats =
  H.parentComponent
    { initialState: const initialState
    , render
    , eval
    , receiver: const Nothing
    }
  where

  initialState :: State
  initialState =
    { toggleCount: 0
    , buttonState: Nothing }

  render :: State -> H.ParentHTML Query Button.Query Slot m
  render state =
    HH.div_
      [ Stats.renderPlayersResponse stats
      , HH.slot ButtonSlot Button.myButton unit (HE.input HandleButton)
      , HH.p_
          [ HH.text ("Button has been toggled " <> show state.toggleCount <> " time(s)") ]
      , HH.p_
          [ HH.text
              $ "Last time I checked, the button was: "
              <> (maybe "(not checked yet)" (if _ then "on" else "off") state.buttonState)
              <> ". "
          , HH.button
              [ HE.onClick (HE.input_ CheckButtonState) ]
              [ HH.text "Check now" ]
          ]
      ]

  eval :: Query ~> H.ParentDSL State Query Button.Query Slot Void m
  eval = case _ of
    HandleButton (Button.Toggled _) next -> do
      H.modify_ (\st -> st { toggleCount = st.toggleCount + 1 })
      pure next
    CheckButtonState next -> do
      buttonState <- H.query ButtonSlot $ H.request Button.IsOn
      H.modify_ (_ { buttonState = buttonState })
      pure next


--   io.subscribe $ CR.consumer \(Button.Toggled newState) -> do
--     log $ "Button was toggled to: " <> show newState
--     pure Nothing
--
--   io.query $ H.action $ HandleButton Button.Toggle
--   io.query $ H.action $ HandleButton Button.Toggle
--   io.query $ H.action $ HandleButton Button.Toggle
--   io.query $ H.action $ HandleButton Button.Toggle

-- import Stats.Models (GameData(..))
-- import Debug.Trace (spy)
-- import Effect.Console (log)

-- main :: Effect Unit
-- main = do
--   log "Hello Josh + Daniel + weiorjweroiwejroiweroiwej!"
--   HA.runHalogenAff (HA.awaitBody >>= runUI myButton unit)
