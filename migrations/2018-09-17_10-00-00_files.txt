Description: Initial schema
Created: 2018-09-10 20:00:00.000000 UTC
Depends: 
Apply: |
    CREATE TABLE folders (
       id SERIAL PRIMARY KEY,
       team_id bigint REFERENCES teams(id),
       parent_id bigint NOT NULL REFERENCES folders(id),
       name character varying NOT NULL,
       Unique (parent_id, name)
    );

    CREATE TABLE files (
       id SERIAL PRIMARY KEY,
       team_id bigint NOT NULL REFERENCES teams(id),
       folder_id bigint NOT NULL REFERENCES folders(id),
       original_file_name character varying NOT NULL
    );

    insert into folders values (0, null, 0, '/');
    insert into folders values (1, 0, 0, 'Proverlays');
    insert into folders values (2, 1, 0, 'ECL');

Revert: |
    DROP TABLE IF EXISTS files;
    DROP TABLE IF EXISTS folders;
