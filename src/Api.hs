
module Api (app) where

import           Conduit                  ( MonadThrow )
import           Control.Monad.Except     ( MonadIO, liftIO, throwError )
import           ServantHelpers
import           Servant.Auth.Server      hiding (throwAll)

import           Auth.Models              ( User )
import           Auth.TeamAPI             ( TeamAPI, teamServer )
import           Auth.UserAPI             ( LoginAPI, UserAPI, loginServer, userServer )
import           Config                   ( App, AppT, Config (..), runAppT )
import           Error                    ( AppError(..), AuthError(..), toServantErr, throwAll )
import           FileServer.FileServerAPI ( FileServerAPI, fileServer )
import           Overlays.AdminAPI        ( AdminAPI, adminServer )
import           Overlays.SceneAPI        ( SceneAPI, sceneServer )
import           Overlays.WebsocketAPI    ( ProtectedWebSocketAPI, UnprotectedWebSocketAPI
                                          , unprotectedWebsocketApi
                                          , unprotectedWebsocketServer
                                          , protectedWebsocketServer )
import           Stats.StatsAPI           ( UnprotectedStatsAPI, unprotectedStatsServer )

type TwitchAOEAPI' auths = (Auth auths User :> Protected) :<|> Unprotected
type TwitchAOEAPI        = TwitchAOEAPI' '[Cookie, JWT]

type Protected = Compose ProtectedServer

-- | Lives behind authorization. Only logged in users can visit these pages.
data ProtectedServer route = ProtectedServer {
    protectedServerTeamApi :: route :- TeamAPI
  , protectedServerUserApi :: route :- UserAPI
  , protectedServerSceneApi :: route :- SceneAPI
  , protectedServerWebsocketApi :: route :- ProtectedWebSocketAPI
  , protectedServerFileServerApi :: route :- FileServerAPI
  , protectedServerAdminApi :: route :- AdminAPI
  } deriving Generic

type Unprotected = Compose UnprotectedServer

-- | Not protected by any authorization. Anyone can visit these pages.
data UnprotectedServer route = UnprotectedServer {
    unrotectedServerLoginApi :: route :- LoginAPI
  , unrotectedServerStatsApi :: route :- UnprotectedStatsAPI
  } deriving Generic

-- |
unprotectedServer :: (MonadThrow m, MonadIO m) => ServerT Unprotected (AppT m)
unprotectedServer = toServant $ UnprotectedServer {..}
    where
    unrotectedServerLoginApi = loginServer
    unrotectedServerStatsApi = unprotectedStatsServer

-- |
protectedServer :: MonadIO m => AuthResult User -> ServerT Protected (AppT m)
protectedServer (Authenticated u) = toServant $ ProtectedServer {..}
    where
    protectedServerTeamApi = teamServer u
    protectedServerUserApi = userServer u
    protectedServerSceneApi = sceneServer u
    protectedServerWebsocketApi = protectedWebsocketServer u
    protectedServerFileServerApi = fileServer u
    protectedServerAdminApi = adminServer u
protectedServer _ = throwAll (AppAuthError NoAuthError)

-- | The main application for the Proverlays backend.
app :: Config -> Application
app cfg = serveWithContext
            (Proxy :: Proxy (TwitchAOEAPI :<|> UnprotectedWebSocketAPI :<|> Raw))
            (_configCookies cfg :. _configJWT cfg :. EmptyContext)
            (mainServer :<|> unprotectedWebsocketServer' :<|> serveDirectoryFileServer "frontend")
    where
    convertApp :: Config -> App a -> Handler a
    convertApp cfg' appt = Handler $
        liftIO (runAppT appt cfg') >>= either (throwError . toServantErr) return

    mainServer :: Server TwitchAOEAPI
    mainServer = hoistServerWithContext
        (Proxy :: Proxy TwitchAOEAPI)
        (Proxy :: Proxy '[CookieSettings, JWTSettings])
        (convertApp cfg)
        (protectedServer :<|> unprotectedServer)

    -- websocketServer doesn't work with Servant.Auth.Server, so it has its own auth.
    unprotectedWebsocketServer' :: Server UnprotectedWebSocketAPI
    unprotectedWebsocketServer' =
        hoistServer unprotectedWebsocketApi (convertApp cfg) unprotectedWebsocketServer
