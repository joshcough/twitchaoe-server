
module PSBridge where

import qualified Auth.PSBridge     as Auth
import qualified Overlays.PSBridge as Overlays
import qualified Stats.PSBridge    as Stats

main :: IO ()
main = do
    Auth.main
    Overlays.main
    Stats.main
