{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Overlays.DatabaseModels where

import           Data.Aeson                       (Value)
import           Database.Persist.Postgresql.JSON ()
import           Database.Persist.TH              (mkDeleteCascade, mkPersist, persistLowerCase, share, sqlSettings)
import           Data.Text                        (Text)

import           Auth.DatabaseModels              (DbUserId)
import           Overlays.Models                  (Dimensions(..))

share [mkPersist sqlSettings, mkDeleteCascade sqlSettings] [persistLowerCase|
DbScene json sql=scenes
    userId     DbUserId
    name       Text
    dimensions Dimensions
    settings   Value
    deriving Show Eq

DbSceneItem json sql=scene_items
    sceneId DbSceneId
    body    Value
    deriving Show Eq

DbStaticItem json sql=static_items
    userId DbUserId
    type   Text
    body   Value
    deriving Show Eq
|]
