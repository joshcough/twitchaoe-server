
module Overlays.SceneAPI where

import           Control.Monad               (forM_)
import           Control.Monad.Except        (MonadIO)
import           Data.Aeson                  (Value, (.=))
import           Data.Text                   (Text)
import           Database.Persist.Postgresql (fromSqlKey, toSqlKey)

import           Auth.DatabaseModels         (DbTeamId)
import           Auth.Models                 (User(..))
import           Auth.UserAPI                (getUserByIdUNSAFE)
import           Config                      (AppT)
import           Logging                     (logDebug)
import           Overlays.DatabaseModels     (DbScene (..), DbSceneItem (..), DbSceneId)
import           Overlays.Models             ( CreateScene(..), CreateSceneResponse(..), Dimensions(..), Scene(..)
                                             , SceneItem(..), UpdateScene(..), UpdateSceneResponse(..)
                                             )
import qualified Overlays.SceneStorage as Db
import           ServantHelpers
import           Util.Utils                  (jsonPathV, setJsonPath')

type SceneAPI = "teams" :> Capture "teamId" DbTeamId :> "scenes" :> Compose SceneServer

data SceneServer route = SceneServer {
    sceneServerGetScenes     :: route :- Get '[JSON] [Scene]
  , sceneServerGetScene      :: route :- Capture "sceneId" DbSceneId :> Get     '[JSON] Scene
  , sceneServerDeleteScene   :: route :- Capture "sceneId" DbSceneId :> Delete  '[JSON] ()
  , sceneServerCreateScene   :: route :- ReqBody '[JSON] CreateScene :> Post    '[JSON] CreateSceneResponse
  , sceneServerUpdateScene   :: route :- ReqBody '[JSON] UpdateScene :> Put     '[JSON] UpdateSceneResponse
  , sceneServerGetSetting    :: route :- Capture "sceneId" DbSceneId :> Capture "path" Text :> Get '[JSON] (Maybe Value)
  , sceneServerUpdateSetting :: route :- Capture "sceneId" DbSceneId :> Capture "path" Text :> ReqBody '[JSON] Value :> Post '[JSON] ()
  , sceneServerLite          :: route :- "scenes_lite" :> Get '[JSON] [Scene]
  } deriving Generic

-- | The server that runs the SceneAPI
sceneServer :: MonadIO m => User -> ServerT SceneAPI (AppT m)
sceneServer caller tid = toServant $ SceneServer {..}
    where
    sceneServerGetScenes     = getScenesByTeamId caller tid
    sceneServerGetScene      = getSceneBySceneId caller tid
    sceneServerDeleteScene   = deleteScene caller tid
    sceneServerCreateScene   = createScene caller
    sceneServerUpdateScene   = updateScene caller tid
    sceneServerGetSetting    = getSceneSettingsPath caller tid
    sceneServerUpdateSetting = updateSceneSettingsPath caller tid
    sceneServerLite          = getScenesByTeamIdWithoutSceneItems caller tid

-- | Returns Scene for the team id
getScenesByTeamId :: MonadIO m => User -> DbTeamId -> AppT m [Scene]
getScenesByTeamId caller tid = do
    $(logDebug) "getScenesByTeamId" ["caller" .= caller, "tid" .= tid]
    callerIsOnTeamOrIsAdminElse401 caller tid $ Db.getScenesByTeamId tid

-- | Returns Scene for the scene id
getSceneBySceneId :: MonadIO m => User -> DbTeamId -> DbSceneId -> AppT m Scene
getSceneBySceneId caller tid sid = do
    $(logDebug) "getSceneBySceneId" ["caller" .= caller, "tid" .= tid, "sid" .= sid]
    withSceneAccess caller tid sid return

-- | deletes a scene from the database.
deleteScene :: MonadIO m => User -> DbTeamId -> DbSceneId -> AppT m ()
deleteScene caller tid sid = do
    $(logDebug) "deleteScene" ["caller" .= caller, "tid" .= tid, "sid" .= sid]
    withSceneAccess caller tid sid $ \_ -> Db.deleteSceneById sid

-- | Creates a scene in the database.
createScene :: MonadIO m => User -> CreateScene -> AppT m CreateSceneResponse
createScene caller c@(CreateScene name settings items) = do
    $(logDebug) "getSceneBySceneId" ["caller" .= caller, "scene" .= c]
    newSceneId <- Db.insertScene $ DbScene (toSqlKey $ userId caller) name (Dimensions 0 0) settings
    ids <- traverse (Db.insertSceneItem . DbSceneItem newSceneId) items
    return $ CreateSceneResponse (fromSqlKey newSceneId) (fromSqlKey <$> ids)

-- | update a scene in the database.
updateScene :: MonadIO m => User -> DbTeamId -> UpdateScene -> AppT m UpdateSceneResponse
updateScene caller tid u@(UpdateScene sid _ _ items) = do
    $(logDebug) "updateScene" ["caller" .= caller, "tid" .= tid, "update" .= u]
    withSceneAccess caller tid (toSqlKey sid) $ \scene -> do
        Db.updateScene u
        forM_ (sceneItems scene) (Db.deleteSceneItemById . toSqlKey . sceneItemId)
        ids <- traverse (Db.insertSceneItem . DbSceneItem (toSqlKey sid)) items
        return $ UpdateSceneResponse (fromSqlKey <$> ids)

-- | lookup a path in the scenes settings json
getSceneSettingsPath :: MonadIO m => User -> DbTeamId -> DbSceneId -> Text -> AppT m (Maybe Value)
getSceneSettingsPath caller tid sid path = do
    $(logDebug) "getSceneSettingsPath" ["caller" .= caller, "tid" .= tid, "path" .= path]
    withSceneAccess caller tid sid $ return . jsonPathV path

-- | update a path in the scenes settings json
updateSceneSettingsPath :: MonadIO m => User -> DbTeamId -> DbSceneId -> Text -> Value -> AppT m ()
updateSceneSettingsPath caller tid sid path newValue = do
    $(logDebug) "updateSceneSettingsPath" ["caller" .= caller, "tid" .= tid, "path" .= path, "newValue" .= newValue]
    withSceneAccess caller tid sid $ \s -> do
        let newValue' = setJsonPath' path newValue $ sceneSettings s
        Db.updateScene $ UpdateScene (sceneId s) (sceneName s) newValue' []

-- | Returns Scene for the team id
getScenesByTeamIdWithoutSceneItems :: MonadIO m => User -> DbTeamId -> AppT m [Scene]
getScenesByTeamIdWithoutSceneItems caller tid = do
    $(logDebug) "getScenesByTeamIdWithoutSceneItems" ["caller" .= caller, "tid" .= tid]
    callerIsOnTeamOrIsAdminElse401 caller tid $ Db.getScenesByTeamIdWithoutSceneItems tid

---
---
---

-- |
withSceneAccess :: (MonadIO m) => User -> DbTeamId -> DbSceneId -> (Scene -> AppT m a) -> AppT m a
withSceneAccess caller tid sid m = callerIsOnTeamOrIsAdminElse401 caller tid $ do
    mScene <- Db.getSceneBySceneIdOnly sid
    maybeOr404 mScene $ \scene -> do
        sceneOwner <- getUserByIdUNSAFE . toSqlKey $ sceneUserId scene
        guard401 (userIsAdmin caller || userTeamId caller == userTeamId sceneOwner) (m scene)
