
module Overlays.WebsocketAPI where

import           Control.Concurrent.Chan      (Chan, readChan, writeChan)
import           Control.Monad.Except         (MonadIO, liftIO)
import           Control.Monad.Reader         (asks)

import           Data.Aeson.Encode.Pretty     (encodePretty)


import           Network.WebSockets           (Connection, forkPingThread, sendTextData)
import           Servant.API.WebSocket        (WebSocket)

import           Auth.Models                  (User (..), UserToken (..))
import           Config                       (AppT, Config (..))
import           Overlays.Models              (WebsocketMessage (..))
import           ServantHelpers

-- not protected by servant-auth, or at all right now.
type UnprotectedWebSocketAPI = "stream" :> Compose UnprotectedWebSocket

newtype UnprotectedWebSocket route = UnprotectedWebSocket {
    unprotectedWebSocketAuth :: route :- Header "Authorization" UserToken :> WebSocket
  } deriving Generic

-- protected by servant-auth
type ProtectedWebSocketAPI = "stream" :> Compose ProtectedWebSocket

newtype ProtectedWebSocket route = ProtectedWebSocket {
    protectedWebSocketSend :: route :-"send" :> ReqBody '[JSON] WebsocketMessage :> Post '[JSON] ()
  } deriving Generic

unprotectedWebsocketApi :: Proxy UnprotectedWebSocketAPI
unprotectedWebsocketApi = Proxy

unprotectedWebsocketServer :: MonadIO m => ServerT UnprotectedWebSocketAPI (AppT m)
unprotectedWebsocketServer = toServant $ UnprotectedWebSocket websocket

protectedWebsocketServer :: MonadIO m => User -> ServerT ProtectedWebSocketAPI (AppT m)
protectedWebsocketServer caller = toServant $ ProtectedWebSocket (send caller)

websocket :: MonadIO m => Maybe UserToken -> Connection -> AppT m ()
-- websocket Nothing _ = throwError err401
websocket _ conn = do
  chan <- getWsEnv
  liftIO $ do
      forkPingThread conn 10
      loop chan
  where
    loop :: Chan WebsocketMessage -> IO ()
    loop chan = do
        wsm <- readChan chan
        sendTextData conn $ encodePretty wsm
        loop chan

send :: MonadIO m => User -> WebsocketMessage -> AppT m ()
send caller wsm = guard401 (userId caller == wsmUserId wsm) (writeToChan wsm)

getWsEnv :: MonadIO m => AppT m (Chan WebsocketMessage)
getWsEnv = asks _configWebSocketEnv

writeToChan :: MonadIO m => WebsocketMessage -> AppT m ()
writeToChan wsm = getWsEnv >>= liftIO . flip writeChan wsm

{-
import qualified Crypto.JOSE                  as Jose
import qualified Crypto.JWT                   as Jose
import qualified Data.ByteString.Lazy         as BSL
import           Data.Int                     (Int64)
import qualified Data.Text                    as T
import qualified Data.Text.Encoding           as T
import           Servant.Auth.Server          (JWTSettings, decodeJWT)
import           Servant.Auth.Server.Internal.ConfigTypes (jwtSettingsToJwtValidationSettings, signingKey)

getJWTSettings :: MonadIO m => AppT m JWTSettings
getJWTSettings = _configJWT <$> ask

readUserFromJwt :: MonadIO m => UserToken -> AppT m User
readUserFromJwt (UserToken (Token token)) = do
    config <- getJWTSettings
    verifiedJWT <- liftIO $ runExceptT $ do
        let token' = BSL.fromStrict . T.encodeUtf8 $ T.drop (T.length "Bearer ") token
        --$(logDebug) . T.pack $ "calling decodeCompact with token: " ++ show token'
        unverifiedJWT <- Jose.decodeCompact token'
        --$(logDebug) $ "calling verifyClaims"
        Jose.verifyClaims (jwtSettingsToJwtValidationSettings config) (signingKey config) unverifiedJWT
    case verifiedJWT of
        Left (_ :: Jose.JWTError) -> do
            --$(logDebug) . T.pack $ "case Jose.JWTError: " ++ show e
            throwError err401
        Right claimset -> case (decodeJWT claimset :: Either T.Text User) of
          Left _ -> do
            --$(logDebug) . T.pack $ "error decoding JWT:" ++ show err
            throwError err401
          Right v' -> return v'
-}