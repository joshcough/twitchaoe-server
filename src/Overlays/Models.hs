{-# LANGUAGE DeriveAnyClass #-}

module Overlays.Models (
    CreateScene(..)
  , CreateSceneResponse(..)
  , Dimensions(..)
  , Scene(..)
  , SceneItem(..)
  , UpdateScene(..)
  , UpdateSceneResponse(..)
  , WebsocketMessage(..)
  ) where

import           Data.Aeson             (FromJSON, ToJSON, Value)
import           Data.Int               (Int64)
import           Data.Text              (Text, pack)
import           Database.Persist.Class (PersistField(..))
import           Database.Persist.Sql   (PersistFieldSql(..), SqlType(..))
import           Database.Persist.Types (PersistValue(PersistList))
import           GHC.Generics           (Generic)
import           Util.Utils             (HasJsonValue(..))

data Scene = Scene {
    sceneId         :: Int64
  , sceneUserId     :: Int64
  , sceneName       :: Text
  , sceneSettings   :: Value
  , sceneItems      :: [SceneItem]
} deriving (Eq, Generic, Show, ToJSON, FromJSON)

instance HasJsonValue Scene where
    getJson = sceneSettings

data SceneItem = SceneItem {
    sceneItemId   :: Int64
  , sceneItemBody :: Value
} deriving (Eq, Generic, Show, ToJSON, FromJSON)


instance HasJsonValue SceneItem where
    getJson = sceneItemBody

data CreateScene = CreateScene {
    createSceneName       :: Text
  , createSceneSettings   :: Value
  , createSceneItems      :: [Value]
} deriving (Eq, Generic, Show, ToJSON, FromJSON)

data UpdateScene = UpdateScene {
    updateSceneId         :: Int64
  , updateSceneName       :: Text
  , updateSceneSettings   :: Value
  , updateSceneItems      :: [Value]
} deriving (Eq, Generic, Show, ToJSON, FromJSON)

data CreateSceneResponse = CreateSceneResponse {
    createSceneResponseSceneId      :: Int64
  , createSceneResponseSceneItemIds :: [Int64]
} deriving (Eq, Ord, Generic, Show, ToJSON, FromJSON)

newtype UpdateSceneResponse = UpdateSceneResponse {
    updateSceneResponseSceneItemIds :: [Int64]
} deriving (Eq, Ord, Generic, Show, ToJSON, FromJSON)

data Dimensions = Dimensions { width :: Int, height :: Int }
    deriving (Eq, Ord, Generic, Show, ToJSON, FromJSON)

instance PersistFieldSql Dimensions where
    sqlType _ = SqlString

instance PersistField Dimensions where
    toPersistValue (Dimensions w h) = PersistList [toPersistValue w, toPersistValue h]
    fromPersistValue v =
        case fromPersistValue v of
            Right [x,y]  -> Dimensions <$> fromPersistValue x <*> fromPersistValue y
            Left e       -> Left e
            _            -> Left $ pack $ "For Dimensions, Expected 2 item PersistList, received: " ++ show v

data WebsocketMessage = WebsocketMessage {
    wsmEvent   :: Text
  , wsmUserId  :: Int64
  , wsmTeamId  :: Int64
  , wsmMessage :: Value
} deriving (Eq, Generic, Show, ToJSON, FromJSON)
