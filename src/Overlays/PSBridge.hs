
module Overlays.PSBridge where

import Data.Proxy
import Language.PureScript.Bridge
import Language.PureScript.Bridge.SumType (equal, order)
import Overlays.Models

main :: IO ()
main = writePSTypes "frontend/src" (buildBridge defaultBridge) myTypes

myTypes :: [SumType 'Haskell]
myTypes =
  [ let p = (Proxy :: Proxy Dimensions)          in order p (mkSumType p)
  , let p = (Proxy :: Proxy Scene)               in equal p (mkSumType p)
  , let p = (Proxy :: Proxy SceneItem)           in equal p (mkSumType p)
  , let p = (Proxy :: Proxy CreateScene)         in equal p (mkSumType p)
  , let p = (Proxy :: Proxy CreateSceneResponse) in order p (mkSumType p)
  , let p = (Proxy :: Proxy WebsocketMessage)    in equal p (mkSumType p)
  ]
