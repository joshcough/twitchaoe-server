
module Overlays.AdminAPI (AdminAPI, adminServer) where

import           Control.Monad.Except (MonadIO)
import           Data.Foldable        (forM_)
import           Data.Text            (Text)

import           Auth.DatabaseModels  (DbTeamId)
import           Auth.Models          (CreateTeam(..), CreateUser(..), Team(..), User(..))
import           Auth.TeamAPI         (getTeamById, createTeam)
import           Auth.UserAPI         (createUser, getUserById)
import           Config               (AppT)
import           Logging              (logDebug)
import           Overlays.SceneAPI    (createScene, getScenesByTeamId)
import           Overlays.Models      (CreateScene(..), Scene(..), SceneItem(..))
import           ServantHelpers

type AdminAPI = "admin" :> Compose AdminServer

newtype AdminServer route = AdminServer {
    adminServerClone :: route :- "clone" :> Capture "teamId" DbTeamId :> Capture "teamName" Text :> ReqBody '[JSON] CreateUser :> Post '[JSON] DbTeamId
  } deriving Generic

adminServer :: MonadIO m => User -> ServerT AdminAPI (AppT m)
adminServer caller = toServant $ AdminServer {..}
    where adminServerClone = cloneTeam caller

cloneTeam :: MonadIO m => User -> DbTeamId -> Text -> CreateUser -> AppT m DbTeamId
cloneTeam caller tidToClone newTeamName newUserInfo = do
    $(logDebug) "cloneTeam" []
    adminOr401 caller $ do

        -- fetch the settings for team being cloned
        settings <- teamSettings <$> getTeamById caller tidToClone

        -- create the new team
        tid' <- createTeam caller $ CreateTeam newTeamName settings

        -- create the new user.
        uid' <- createUser caller newUserInfo
        u'   <- getUserById caller uid'

        -- get all the scenes for the team we are cloning.
        scenes <- getScenesByTeamId caller tidToClone

        -- clone all the data.
        forM_ scenes (createScene u' . sceneToCreateScene)

        -- finally just return the new team id
        -- we could return all the info from the previous operations, but i don't think its needed.
        return tid'

        where
    sceneToCreateScene Scene{..} = CreateScene sceneName sceneSettings (sceneItemBody <$> sceneItems)
