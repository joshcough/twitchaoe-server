
module Overlays.SceneStorage (
    SceneDb(..)
  ) where

import           Control.Monad.Except        (MonadIO)
import           Database.Esqueleto
import qualified Database.Persist.Postgresql as P

import           Auth.DatabaseModels         (DbUser(..), EntityField( DbUserTeamId, DbUserId ), DbTeamId, DbUserId)
import           Config                      (AppT', runDb)
import           Overlays.DatabaseModels     (EntityField(..), DbSceneId, DbSceneItemId, DbScene (..), DbSceneItem (..))
import           Overlays.Models             (Scene(..), SceneItem(..), UpdateScene(..))

class Monad m => SceneDb m where
    deleteSceneById :: DbSceneId -> m ()
    deleteSceneItemById :: DbSceneItemId -> m ()
    getSceneBySceneIdOnly :: DbSceneId -> m (Maybe Scene)
    getScenesByTeamId :: DbTeamId -> m [Scene]
    getScenesByTeamIdWithoutSceneItems :: DbTeamId -> m [Scene]
    getScenesByUserIdWithoutSceneItems :: DbUserId -> m [Scene]
    insertScene :: DbScene -> m DbSceneId
    insertSceneItem :: DbSceneItem -> m DbSceneItemId
    updateScene :: UpdateScene -> m ()

instance MonadIO m => SceneDb (AppT' e m) where
    deleteSceneById = runDb . deleteSceneById
    deleteSceneItemById = runDb . deleteSceneItemById
    getSceneBySceneIdOnly = runDb . getSceneBySceneIdOnly
    getScenesByTeamId = runDb . getScenesByTeamId
    getScenesByTeamIdWithoutSceneItems = runDb . getScenesByTeamIdWithoutSceneItems
    getScenesByUserIdWithoutSceneItems = runDb . getScenesByUserIdWithoutSceneItems
    insertScene = runDb . insertScene
    insertSceneItem = runDb . insertSceneItem
    updateScene = runDb . updateScene

instance MonadIO m => SceneDb (SqlPersistT m) where
    deleteSceneById = P.deleteCascade

    deleteSceneItemById siid = delete $ from $ \sceneItem -> do
        where_ $ sceneItem ^. DbSceneItemId ==. val siid
        return ()

    getScenesByTeamId tid = do
        scenes <- select $ from $ \scene -> do
            where_ $ scene ^. DbSceneUserId `in_` selectTeamUserIds tid
            return scene
        items  <- traverse (getDbSceneItemsBySceneId . entityKey) scenes
        return $ zipWith sceneFromEntity scenes items

    -- | select * from scenes s where s.userId in (select * from teams where teamId = ?)
    getScenesByTeamIdWithoutSceneItems tid = fmap (fmap (`sceneFromEntity` [])) $
        select $ from $ \scene -> do
            where_ $ scene ^. DbSceneUserId `in_` selectTeamUserIds tid
            return scene

    getScenesByUserIdWithoutSceneItems uid =
        fmap (fmap $ flip sceneFromEntity []) (P.selectList [DbSceneUserId P.==. uid] [])

    updateScene (UpdateScene sid name settings _) =
        P.update (toSqlKey sid) [ DbSceneName P.=. name, DbSceneSettings P.=. settings ]

    getSceneBySceneIdOnly sid = do
        mScene <- getEntity sid
        items  <- getDbSceneItemsBySceneId sid
        return $ uncurry sceneFromEntity . (\s -> (s, items)) <$> mScene

    insertScene = insert
    insertSceneItem = insert

-- |
sceneFromEntity :: Entity DbScene -> [Entity DbSceneItem] -> Scene
sceneFromEntity (Entity k (DbScene uk name _ settings)) items =
    Scene (fromSqlKey k) (fromSqlKey uk) name settings (sceneItemFromEntity <$> items)
    where
    sceneItemFromEntity :: Entity DbSceneItem -> SceneItem
    sceneItemFromEntity (Entity k' v') = SceneItem (fromSqlKey k') (dbSceneItemBody v')

-- |
getDbSceneItemsBySceneId :: (MonadIO m) => DbSceneId -> SqlPersistT m [Entity DbSceneItem]
getDbSceneItemsBySceneId sid = P.selectList [DbSceneItemSceneId P.==. sid] []

-- TODO: this should probably go in a TeamStorage module or something.
selectTeamUserIds :: From query expr backend (expr (Entity DbUser)) => DbTeamId -> expr (ValueList (Key DbUser))
selectTeamUserIds tid = subList_select $ from $ \user -> do
    where_ $ user ^. DbUserTeamId ==. val tid
    return $ user ^. DbUserId
