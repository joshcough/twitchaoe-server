
module ServantHelpers (
    module Servant
  , module Servant.Generic
  , Compose
  , toServant
  , guard401
  , maybeOr401
  , maybeOr404
  , maybeOr500
  , adminOr401
  , callerIsUserOr401
  , callerIsUserOrIsAdminElse401
  , callerIsOnTeamOrIsAdminElse401
  , unexpected
  ) where

import           Control.Monad.Except        (MonadError)
import           Database.Persist.Postgresql (fromSqlKey)
import           Data.Text                   (Text, unpack)
import           Servant
import           Servant.Generic      hiding (toServant)
import qualified Servant.Generic as S


import           Auth.DatabaseModels         (DbTeamId, DbUserId)
import           Auth.Models                 (User(..))
import           Error                       (AppError(..), AuthError(..), ProverlaysError, ProverlaysError'(..))

---
--- servant generic helpers
---

type Compose api = S.ToServant (api S.AsApi)

toServant :: forall m api. S.GenericProduct (api (S.AsServerT m)) => api (S.AsServerT m) -> S.ToServant (api (S.AsServerT m))
toServant = S.toServant

---
--- error and access handling
---

unexpected :: MonadError ProverlaysError m => Text -> m a
unexpected t = throwError . AppAppError . ProverlaysMiscError $ unpack t

guard401 :: MonadError ProverlaysError m => Bool -> m a -> m a
guard401 b m = if b then m else throwError $ AppNotFoundError ""

maybeOr404 :: MonadError ProverlaysError m => Maybe a -> (a -> m b) -> m b
maybeOr404 = maybeOrErr $ AppNotFoundError ""

maybeOr401 :: MonadError ProverlaysError m => Maybe a -> (a -> m b) -> m b
maybeOr401 = maybeOrErr (AppAuthError NoAuthError)

maybeOr500 :: MonadError ProverlaysError m => Text -> Maybe a -> (a -> m b) -> m b
maybeOr500 msg = maybeOrErr (AppAppError $ ProverlaysMiscError $ unpack msg)

maybeOrErr :: MonadError ProverlaysError m => ProverlaysError -> Maybe a -> (a -> m b) -> m b
maybeOrErr err = flip $ maybe (throwError err)

adminOr401 :: MonadError ProverlaysError m => User -> m a -> m a
adminOr401 u m = if userIsAdmin u then m else throwError (AppAuthError NoAuthError)

callerIsUserOr401 :: MonadError ProverlaysError m => User -> DbUserId -> m a -> m a
callerIsUserOr401 caller uid m = if userId caller == fromSqlKey uid then m else throwError (AppAuthError NoAuthError)

callerIsUserOrIsAdminElse401 :: MonadError ProverlaysError m => User -> DbUserId -> m a -> m a
callerIsUserOrIsAdminElse401 caller uid m =
    if userIsAdmin caller || userId caller == fromSqlKey uid then m else throwError (AppAuthError NoAuthError)

callerIsOnTeamOrIsAdminElse401 :: MonadError ProverlaysError m => User -> DbTeamId -> m a -> m a
callerIsOnTeamOrIsAdminElse401 caller tid m =
    if userIsAdmin caller || userTeamId caller == fromSqlKey tid then m else throwError (AppAuthError NoAuthError)
