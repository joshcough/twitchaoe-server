{-# LANGUAGE DeriveAnyClass    #-}

module Stats.GameViews (
    GameView(..)
  , isTeamGame
  , viewPlayer
  , viewTeam
  , viewMap
  , viewCiv
  , viewTeamGames
  , view1v1s
  , viewMirrors
  --
  ) where

import           Control.Monad              (guard)
import           Data.Aeson                 (FromJSON, ToJSON)
import           Data.Maybe                 (mapMaybe)
import           Data.Tuple.Utils           (fst3, snd3)
import           GHC.Generics               (Generic)
import           Stats.Models               (Civilization(..), Game(..), GameData(..), GameDataBody(..), GameMap(..), AoeTeam(..), Player(..), Tag(..))

data GameView =
    MapView GameMap
  | PlayerView Player
  | AoeTeamView AoeTeam
  | CivView Civilization
  | TeamGameView
  | OneVOneView
  | MirrorView
  | TagView Tag
  | AndView GameView GameView
  | OrView  GameView GameView
  deriving (Eq, Show, Generic, ToJSON, FromJSON)

viewGames :: GameView -> [Game] -> [Game]
viewGames v = mapMaybe (`viewGame` v)

viewMap :: GameMap -> [Game] -> [Game]
viewMap m = viewGames (MapView m)

viewPlayer :: Player -> [Game] -> [Game]
viewPlayer p = viewGames (PlayerView p)

viewTeam :: AoeTeam -> [Game] -> [Game]
viewTeam p = viewGames (AoeTeamView p)

viewCiv :: Civilization -> [Game] -> [Game]
viewCiv c = viewGames (CivView c)

viewTeamGames :: [Game] -> [Game]
viewTeamGames = viewGames TeamGameView

view1v1s :: [Game] -> [Game]
view1v1s = viewGames OneVOneView

viewMirrors :: [Game] -> [Game]
viewMirrors = viewGames MirrorView

viewGame :: Game -> GameView -> Maybe Game
viewGame g@Game{} v = guard (shouldViewGame g v) >> case v of
    MapView _     -> pure g
    PlayerView p  -> pure . filtered $ (==p) . fst3
    AoeTeamView t -> pure . filtered $ (== Just t) . playerTeam . fst3
    CivView c     -> pure . filtered $ (==c) . snd3
    TeamGameView  -> pure g
    OneVOneView   -> pure g
    MirrorView    -> pure g
    TagView _     -> pure g
    AndView a b   -> viewGame g a >>= flip viewGame b
    OrView  a b   -> viewGame g a >>= flip viewGame b
    where
    filtered f = g { gamePlayerStats = filter f $ gamePlayerStats g }

shouldViewGame :: Game -> GameView -> Bool
shouldViewGame g@Game{} v = case v of
    MapView m     -> gameDataGameMap g == m
    PlayerView p  -> p `elem` gamePlayers g
    AoeTeamView t -> t `elem` gameTeams   g
    CivView c     -> c `elem` gameCivs    g
    TeamGameView  -> isTeamGame g
    OneVOneView   -> isOneVOne  g
    MirrorView    -> isMirror   g
    TagView t     -> t `elem` gameTags g
    AndView a b   -> shouldViewGame g a && shouldViewGame g b
    OrView  a b   -> shouldViewGame g a || shouldViewGame g b

-- | A list of _distinct_ players in a game.
gamePlayers :: Game -> [Player]
gamePlayers = fmap fst3 . gamePlayerStats

-- | A list of _distinct_ players in a game.
numberOfPlayers :: Game -> Int
numberOfPlayers = num_players . gameDataBody . gameGameData

-- | A list of _distinct_ teams in a game.
gameTeams :: Game -> [AoeTeam]
gameTeams = mapMaybe (playerTeam . fst3) . gamePlayerStats

-- | A list of _distinct_ civs in a game.
gameCivs :: Game -> [Civilization]
gameCivs = fmap snd3 . gamePlayerStats

-- |
isOneVOne :: Game -> Bool
isOneVOne g = numberOfPlayers g == 2

-- | TODO: this isn't technically right, but for what we want, it works fine.
isTeamGame :: Game -> Bool
isTeamGame = not . isOneVOne

-- |
isMirror :: Game -> Bool
isMirror = (==1) . length . gameCivs
