{-# LANGUAGE GeneralizedNewtypeDeriving #-}

{-# OPTIONS_GHC -fno-warn-orphans #-}

-- | Test
module Stats.Html (
    AggregatedCivStatsResponse(..)
  , AggregatedMapStatsResponse(..)
  , AggregatedPlayerStatsResponse(..)
  , AggregatedTeamStatsResponse(..)
  , CivList(..)
  , civHtml
  , civList
  , index
) where

import           Prelude                        hiding (div, head, id)
import           Control.Monad                  (forM_, when)
import           Data.Aeson                     (FromJSON, ToJSON)
import           Data.Int                       (Int64)
import           Data.Map                       (Map)
import qualified Data.Map                    as Map
import           Data.Maybe                     (mapMaybe)
import           Data.Text                      (Text, unpack)
import           GHC.Generics                   (Generic)
import           Text.Blaze                     (Markup)
import           Text.Blaze.Html                (toHtml)
import           Text.Blaze.Html5            hiding (style)
import           Text.Blaze.Html5.Attributes hiding (form, title, label)
import           Text.Printf                    (PrintfArg, printf)
import           Stats.Models                   ( Civilization(..), Game(..), GameData(..), GameDataBody(..), Player(..), PlayerStats(..)
                                                , AoeTeam(..), GameMap(..), Time(..), Uptimes(..), civsById)
import           Stats.Aggregations             ( AggregatedStats(..), AggregatedStatsGroup(..), AggregatedMapStatsGroup(..)
                                                , AggregatedCivStats(..), AggregatedMapStats(..), AggregatedCommonStats(..)
                                                , AggregatedPlayerStats(..), AggregatedTeamStats(..))
import           Util.Utils                     (strength)

-- |
newtype CivList = CivList {
    civListCivs :: Map Int64 Civilization
} deriving (Eq, Ord, Show, Generic, ToJSON, FromJSON)

civList :: CivList
civList = CivList civsById

-- |
instance ToMarkup CivList where
    toMarkup (CivList civs) = docTypeHtml $ do
        head $ title "Civs"
        body $ do
            p "AOE2 Civilizations"
            ul $ forM_ (snd <$> Map.toList civs) (li . civHtml)

-- |
civHtml :: Civilization -> Html
civHtml c = div $ do
    p . toHtml $ civName c
    civImage c

civImage :: Civilization -> Html
civImage c = img ! (src . stringValue $ path)
    where path = "/images/civs/" ++ unpack (civName c) ++ ".png"

instance ToMarkup Civilization where
    toMarkup = civHtml

index :: Html
index = docTypeHtml $ do
    head $ title "Twitch AOE"
    body $ do
        h1 "Twitch AOE"
        p  "Upload AOE2 recordings"
        h2 "Upload a single game"
        recordingsForm "/stats/hidden/stats/upload"     "postRecording"  "Select a file to upload"
        h2 "Upload a split game"
        splitRecordingsForm
        h2 "Upload a zip file full of games"
        recordingsForm "/stats/hidden/stats/upload/zip" "postRecordings" "Select a zip file to upload"
    where
    recordingsForm loc n labelText =
        form ! action loc ! method "post" ! name n ! enctype "multipart/form-data" $ do
            fileInput n
            label ! for n $ labelText
            br
            submitButton

    splitRecordingsForm =
        form ! action "/stats/hidden/stats/upload/split"  ! method "post" ! name "postRecordingSplit" ! enctype "multipart/form-data" $ do
            fileInput "postRecordingSplit"
            label ! for "postRecordingSplit" $ "Select first file to upload"
            br
            fileInput "postRecordingSplit"
            label ! for "postRecordingSplit" $ "Select second file to upload"
            br
            submitButton
    fileInput inputName = input ! type_ "file" ! name inputName ! value inputName ! id inputName
    submitButton = input ! type_ "submit" ! value "submit"

{-class AggregatedStatsResponse s where
    statsList  :: s -> [a]
    statsTitle :: s -> String-}

newtype AggregatedPlayerStatsResponse = AggregatedPlayerStatsResponse {
    aggPlayerStatsRespStats :: [AggregatedPlayerStats]
} deriving (Eq, Show, Generic, ToJSON, FromJSON)

instance ToMarkup AggregatedPlayerStatsResponse where
    toMarkup (AggregatedPlayerStatsResponse []) = div $ p "No stats found."
    toMarkup (AggregatedPlayerStatsResponse aggs) = aggHtml "Player" aggs

newtype AggregatedTeamStatsResponse = AggregatedTeamStatsResponse {
    aggTeamStatsRespStats :: [AggregatedTeamStats]
} deriving (Eq, Show, Generic, ToJSON, FromJSON)

instance ToMarkup AggregatedTeamStatsResponse where
    toMarkup (AggregatedTeamStatsResponse []) = div $ p "No stats found."
    toMarkup (AggregatedTeamStatsResponse aggs) = aggHtml "Team" aggs

newtype AggregatedCivStatsResponse = AggregatedCivStatsResponse {
    aggCivStatsRespStats :: [AggregatedCivStats]
} deriving (Eq, Show, Generic, ToJSON, FromJSON)

instance ToMarkup AggregatedCivStatsResponse where
    toMarkup (AggregatedCivStatsResponse []) = div $ p "No stats found."
    toMarkup (AggregatedCivStatsResponse aggs) = aggHtml "Civilization" aggs

instance ToMarkup (Player, AggregatedStats) where
    toMarkup (player, stats) = do
        statsRow' (playerName player)
        aggregatedStatsHtml stats

instance ToMarkup (AoeTeam, AggregatedStats) where
    toMarkup (team, stats) = do
        statsRow' (teamName team)
        aggregatedStatsHtml stats

instance ToMarkup (Civilization, AggregatedStats) where
    toMarkup (civ, stats) = do
        statsRow' (civName civ)
        aggregatedStatsHtml stats

statsHeaders :: Markup
statsHeaders = do
    th "Wins"
    th "Losses"
    th "Win Pct."
    th "MVPs"
    th "K/D Ratio"
    commonHeaders

commonHeaders :: Markup
commonHeaders = do
    th "Games Played"
    th "Avg HP Damage"
    th "Avg Units Killed"
    th "Avg Food Collected"
    th "Avg Wood Collected"
    th "Avg Gold Collected"
    th "Avg Stone Collected"
    th "Avg Tribute Sent"
    th "Avg Tribute Received"
    th "Avg Map Exploration"
    th "Avg Relic Gold"
    th "Avg Game Length"
    th "Avg Feudal Time"
    th "Avg Castle Time"
    th "Avg Imperial Time"

instance ToMarkup AggregatedMapStats where
    toMarkup = aggregatedMapStatsHtml

newtype AggregatedMapStatsResponse = AggregatedMapStatsResponse {
    aggMapStatsRespStats :: [AggregatedMapStatsGroup]
} deriving (Eq, Show, Generic, ToJSON, FromJSON)

instance ToMarkup AggregatedMapStatsResponse where
    toMarkup (AggregatedMapStatsResponse []) = div $ p "No stats found."
    toMarkup (AggregatedMapStatsResponse aggs) = do
        f "Totals"     $ mapMaybe getTotals    aggs
        f "1v1s  "     $ mapMaybe get1v1s      aggs
        f "Team Games" $ mapMaybe getTeamGames aggs
        where
        f :: Text -> [AggregatedMapStats] -> Html
        f header' aggs' = div . table $ do
            h2 $ toHtml header'
            tr $ do th "Map"
                    commonHeaders
            forM_ aggs' (tr . toMarkup)

class StatsGroup a b | a -> b where
    getTotals    :: a -> Maybe b
    get1v1s      :: a -> Maybe b
    getTeamGames :: a -> Maybe b

instance StatsGroup AggregatedPlayerStats (Player, AggregatedStats) where
    getTotals    (AggregatedPlayerStats p' g) = strength (p', aggStatsTotals g)
    get1v1s      (AggregatedPlayerStats p' g) = strength (p', aggStats1v1s   g)
    getTeamGames (AggregatedPlayerStats p' g) = strength (p', aggStatsTgs    g)

instance StatsGroup AggregatedCivStats (Civilization, AggregatedStats) where
    getTotals    (AggregatedCivStats c g) = strength (c, aggStatsTotals g)
    get1v1s      (AggregatedCivStats c g) = strength (c, aggStats1v1s   g)
    getTeamGames (AggregatedCivStats c g) = strength (c, aggStatsTgs    g)

instance StatsGroup AggregatedTeamStats (AoeTeam, AggregatedStats) where
    getTotals    (AggregatedTeamStats t g) = strength (t, aggStatsTotals g)
    get1v1s      (AggregatedTeamStats t g) = strength (t, aggStats1v1s   g)
    getTeamGames (AggregatedTeamStats t g) = strength (t, aggStatsTgs    g)

instance StatsGroup AggregatedStatsGroup AggregatedStats where
    getTotals    = aggStatsTotals
    get1v1s      = aggStats1v1s
    getTeamGames = aggStatsTgs

instance StatsGroup AggregatedMapStatsGroup AggregatedMapStats where
    getTotals    = aggMapStatsTotals
    get1v1s      = aggMapStats1v1s
    getTeamGames = aggMapStatsTgs

aggHtml :: (ToMarkup b, StatsGroup a b) => Text -> [a] -> Html
aggHtml rowTitle aggs = do
    f "Totals"     rowTitle $ mapMaybe getTotals    aggs
    f "1v1s  "     rowTitle $ mapMaybe get1v1s      aggs
    f "Team Games" rowTitle $ mapMaybe getTeamGames aggs
    where
    f :: ToMarkup a => Text -> Text -> [a] -> Html
    f header' rowTitle' aggs' = do
        h2 $ toHtml header'
        div . table $ do
            tr $ do th $ toHtml rowTitle'
                    statsHeaders
            forM_ aggs' (tr . toMarkup)

aggregatedStatsHtml :: AggregatedStats -> Html
aggregatedStatsHtml AggregatedStats {..} = do
    statsRow' statsNumWins
    statsRow' statsNumLosses
    statsRow' $ rounded statsWinPct
    statsRow' statsNumMVPs
    statsRow' $ rounded statsKDRatio
    aggregatedCommonStatsHtml statsCommonStats

aggregatedMapStatsHtml :: AggregatedMapStats -> Html
aggregatedMapStatsHtml AggregatedMapStats {..} = do
    statsRow' $ mapName mapStatsMap
    aggregatedCommonStatsHtml mapStatsCommonStats

aggregatedCommonStatsHtml :: AggregatedCommonStats -> Html
aggregatedCommonStatsHtml AggregatedCommonStats {..} = do
    statsRow'   statsGamesPlayed
    statsRow' $ truncated statsHPDamage
    statsRow' $ truncated statsNumKilled
    statsRow' $ truncated statsAvgFood
    statsRow' $ truncated statsAvgWood
    statsRow' $ truncated statsAvgGold
    statsRow' $ truncated statsAvgStone
    statsRow' $ truncated statsTributeSent
    statsRow' $ truncated statsTributeReceived
    statsRow' $ rounded   statsAvgMapExp
    statsRow' $ truncated statsRelicGold
    statsRow' $ timeHtml  statsAvgGameLength
    uptimesHtml statsAvgUptimes

statsRow' :: ToMarkup a => a -> Html
statsRow' statVal = td ! style "text-align:right" $ toHtml statVal

statsRowL :: ToMarkup a => a -> Html
statsRowL statVal = td ! style "text-align:left" $ toHtml statVal

uptimesHtml :: Uptimes -> Html
uptimesHtml Uptimes{..} = do
    statsRow' $ uptimeMaybe uptimeFeudal
    statsRow' $ uptimeMaybe uptimeCastle
    statsRow' $ uptimeMaybe uptimeImperial
    where
    uptimeMaybe Nothing  = "-"
    uptimeMaybe (Just u) = timeHtml u

timeHtml :: Time -> Html
timeHtml Time{..} = do
    when (hours > 0) $ toHtml hours >> sep
    pad minutes
    sep
    pad seconds
    where
    pad t = toHtml $ (if t < 10 then "0" else "") ++ show t
    sep = toHtml (":" :: String)

rounded :: PrintfArg t => t -> Html
rounded d = toHtml (printf "%.2f" d :: String)

truncated :: RealFrac a => a -> Html
truncated d = toHtml $ show (floor d :: Int)

instance ToMarkup [Game] where
    toMarkup [] = div $ p "No games found."
    toMarkup xs = forM_ xs gameHtml

gameHtml :: Game -> Html
gameHtml g@Game{..} = div $ do
    div . table $ do
        tr gameDataHeaders
        tr $ gameDataHtml g
    div . table $ do
        tr playerHeaders
        forM_  gamePlayerStats (tr . playerStatsHtml)
    br

gameDataHeaders :: Markup
gameDataHeaders = do
    th "Id"
    th "Name"
    th "Map"

playerHeaders :: Markup
playerHeaders = do
    th "Player"
    th "Team"
    th "Civ"
    th "Win"

gameDataHtml :: Game -> Html
gameDataHtml Game {..} = do
    statsRow' $ gameDataId gameGameData
    statsRowL . gameName $ gameDataBody gameGameData
    statsRowL $ mapName gameDataGameMap
    statsRow' . timeHtml . game_length $ gameDataBody gameGameData

playerStatsHtml :: (Player, Civilization, PlayerStats) -> Html
playerStatsHtml (player, civ, PlayerStats {..}) = do
    statsRowL $ playerName player
    statsRowL $ maybe "-" teamName $ playerTeam player
    statsRowL $ civName civ
    statsRowL $ p $ if victory then "Yes" else "No"
