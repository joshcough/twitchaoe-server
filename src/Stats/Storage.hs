
module Stats.Storage (
    StatsDb(..)
  ) where

import           Control.Arrow               ((&&&))
import           Control.Monad               (forM, forM_, join)
import           Control.Monad.Except        (MonadIO)
import           Control.Monad.Trans.Reader  (ReaderT)
import           Database.Persist.Postgresql (Entity (..), fromSqlKey, insert, toSqlKey, getBy)
import qualified Data.List                   as List
import           Data.Map                    (Map)
import qualified Data.Map                    as Map
import           Data.Maybe                  (fromJust)
import           Data.Text                   (Text)
import qualified Data.Text                   as T
import           Database.Esqueleto

import           Config                      (AppT', runDb)
import qualified Stats.DatabaseModels        as Db
import           Stats.DatabaseModels        ( DbGame(..), DbGameId, DbPlayerStats(..), DbCiv(..), DbAoeTeam(..), DbMap(..), DbPlayer(..)
                                             , DbTag(..), EntityField(..), Unique(DbMapUniqueName, DbPlayerUniqueName, DbAoeTeamUniqueName))
import           Stats.Models                ( Civilization(..), Game(..), GameImportResult(..), GameData(..), GameDataBody(..)
                                             , GameMap(..), PlayerStats(..), AoeTeam(..), Player(..), Tag(..), mkTime', unMkTime, civList)

class Monad m => StatsDb m where
    insertResult :: GameImportResult -> m DbGameId
    getDbStatsByPlayer :: Player -> m [Game]
    getDbStatsForAllPlayers :: m (Map Player [Game])
    getDbStatsByTeam :: AoeTeam -> m [Game]
    getDbStatsForAllTeams :: m (Map AoeTeam [Game])
    getDbStatsByCiv  :: Civilization -> m [Game]
    getDbStatsForAllCivs :: m (Map Civilization [Game])
    getDbStatsByMap ::  GameMap -> m [Game]
    getDbStatsForAllMaps :: m (Map GameMap [Game])
    getPlayers :: m [Player]
    getPlayer :: Text -> m (Maybe Player)
    getMap :: Text -> m (Maybe GameMap)
    getMaps :: m [GameMap]
    getTeam :: Text -> m (Maybe AoeTeam)
    getTeams :: m [AoeTeam]
    getAllGames :: m [Game]

instance MonadIO m => StatsDb (AppT' e m) where
    insertResult = runDb . insertResult
    getDbStatsByPlayer = runDb . getDbStatsByPlayer
    getDbStatsForAllPlayers = runDb $ getDbStatsForAllPlayers
    getDbStatsByTeam = runDb . getDbStatsByTeam
    getDbStatsForAllTeams = runDb getDbStatsForAllTeams
    getDbStatsByCiv = runDb . getDbStatsByCiv
    getDbStatsForAllCivs = runDb getDbStatsForAllCivs
    getDbStatsByMap = runDb . getDbStatsByMap
    getDbStatsForAllMaps = runDb getDbStatsForAllMaps
    getPlayer = runDb . getPlayer
    getPlayers = runDb getPlayers
    getMap = runDb . getMap
    getMaps = runDb getMaps
    getTeam = runDb . getTeam
    getTeams = runDb getTeams
    getAllGames = runDb getAllGames

instance MonadIO m => StatsDb (SqlPersistT m) where
    insertResult (GameImportResult gameData maybeMapName playerStats) = do
        mMapKey <- join <$> mapM (fmap (fmap entityKey) . insertMap . DbMap) maybeMapName
        gId     <- insert $ gameDataBodyToDb gameData mMapKey
        forM_ playerStats $ \p -> do
            (mTeamKey, playerKey) <- insertPlayer (playerStatsPlayerName p)
            insert $ playerStatsToDb gId playerKey mTeamKey p
        return gId

    getDbStatsByPlayer player =
        getGames $ \(_, p, _, _, _) -> where_ $ p ^. DbPlayerName ==. val (playerName player)

    getDbStatsForAllPlayers = do
        players <- getPlayers
        games   <- getAllGames
        return . Map.fromList $ (id &&& const games) <$> players

    -- TODO: this one is broken...we don't have a real way to query by team right now :(
    getDbStatsByTeam _ = getAllGames

    getDbStatsForAllTeams = do
        teams <- getTeams
        games <- getAllGames
        return . Map.fromList $ map (id &&& const games) teams

    getDbStatsByCiv civ = getGames $ \(_, _, c, _, _) -> where_ $ c ^. DbCivName ==. val (civName civ)

    getDbStatsForAllCivs = do
        games <- getAllGames
        return . Map.fromList $ (id &&& const games) <$> civList

    getDbStatsByMap GameMap{..} = getGames $ \(_, _, _, _, m) -> where_ $ m ^. DbMapName ==. val (T.toLower mapName)

    getDbStatsForAllMaps = do
        maps  <- getMaps
        games <- getAllGames
        return . Map.fromList $ (id &&& const games) <$> maps

    getPlayers = do
        teams <- getTeams'
        fmap (fmap $ entityToPlayer teams) $ select $ from return

    getPlayer playerName = do
        teams <- getTeams'
        fmap (entityToPlayer teams) <$> getBy (DbPlayerUniqueName playerName)

    getMaps = fmap entityToGameMap <$> select (from return)

    getMap mapName = fmap entityToGameMap <$> getBy (DbMapUniqueName mapName)

    getTeams = Map.elems <$> getTeams'

    getTeam teamName = fmap entityToTeam <$> getBy (DbAoeTeamUniqueName teamName)

    getAllGames = getGames $ \_ -> pure ()

---
--- helper functions
---

-- |
getTeams' :: MonadIO m => SqlPersistT m (Map (Key DbAoeTeam) AoeTeam)
getTeams' = Map.fromList . fmap (entityKey &&& entityToTeam) <$> select (from return)

type S a = SqlExpr (Entity a)

-- | Get games from the db
getGames :: (MonadIO m) =>
         ((S DbPlayerStats, S DbPlayer, S DbCiv, S DbGame, S DbMap) -> SqlQuery a)
      -> ReaderT SqlBackend m [Game]
getGames whereClause = do
    players <- getPlayers
    let playerMap = Map.fromList $ (playerName &&& id) <$> players
    res <- select $
        from $ \(s, p, c, g, m) -> do
        where_ (
                (g ^. DbGameMapId)               ==. just (m ^. DbMapId)
            &&. (s ^. DbPlayerStatsPlayer_id)    ==. (p ^. DbPlayerId)
            &&. (s ^. DbPlayerStatsCivilization) ==. (c ^. DbCivId)
            &&. (s ^. DbPlayerStatsGameId)       ==. (g ^. DbGameId)
         )
        _ <- whereClause (s, p, c, g, m)
        return (s, p, c, g, m)
    mkGames playerMap res

mkGames :: MonadIO m =>
          Map Text Player
       -> [(Entity DbPlayerStats, Entity DbPlayer, Entity DbCiv, Entity DbGame, Entity DbMap)]
       -> ReaderT SqlBackend m [Game]
mkGames playersMap rows = forM (List.groupBy gameDataGrouping . List.sortBy gameDataSorting . fmap resolve $ rows) mkGame
    where
    resolve (s, p, c, g, m) = (
        entityToPlayerStats s c $ dbPlayerName $ entityVal p
      , fromJust $ Map.lookup (dbPlayerName $ entityVal p) playersMap
      , entityToCiv c
      , entityToGameData g
      , entityToGameMap m
      )

    gameDataSorting  (_, _, _, g1, _) (_, _, _, g2, _) = compare (gameName . gameDataBody $ g1) (gameName . gameDataBody $ g2)
    gameDataGrouping (_, _, _, g1, _) (_, _, _, g2, _) = gameName (gameDataBody g1) == gameName (gameDataBody g2)

    mkGame :: (MonadIO m) => [(PlayerStats, Player, Civilization, GameData, GameMap)] -> ReaderT SqlBackend m Game
    mkGame rows' = Game g m (f <$> rows') <$> getTags g
        where
        g = (\(_, _, _, g', _) -> g') $ head rows'
        m = (\(_, _, _, _, m') -> m') $ head rows'
        f (s, p, c, _, _) = (p, c, s)

    getTags :: (MonadIO m) => GameData -> ReaderT SqlBackend m [Tag]
    getTags GameData{..} = fmap (List.nub . fmap entityToTag) $ select $ from $ \(tag, gameTag) -> do
        where_ (
                (tag     ^. DbTagId)         ==. (gameTag ^. DbGameTagTagId)
            &&. (gameTag ^. DbGameTagGameId) ==. val (toSqlKey gameDataId)
         )
        return tag

---
---
---

-- | Only inserts the player if it doesn't already exist in the db
insertPlayer :: MonadIO m => Text -> SqlPersistT m (Maybe (Key DbAoeTeam), Key DbPlayer)
insertPlayer playerName = do
    -- Look up the player in the database by name.
    -- if it is there, return it.
    -- if not, insert the player and its team (also if the team doesn't already exist)
    let (maybeTeamName, playerName') = playerFromName playerName
    getPlayer playerName' >>= \case
        Just p -> return (toSqlKey . teamId <$> playerTeam p, toSqlKey $ playerId p)
        Nothing -> do
            tKey  <- join <$> mapM (insertTeam . DbAoeTeam) maybeTeamName
            pKey  <- insertUnique $ DbPlayer tKey playerName'
            pKey' <- maybe (entityKey . fromJust <$> getBy (DbPlayerUniqueName playerName')) return pKey
            return (tKey, pKey')

-- |
playerFromName :: Text -> (Maybe Text, Text)
playerFromName n =
    if T.head n == '['
    then case T.split (==']') n of
        [team, player] -> (Just $ T.drop 1 team, player)
        _weirdness     -> error $ "Could not parse player name: " ++ T.unpack n
    else (Nothing, n)

-- |
insertTeam :: MonadIO m => DbAoeTeam -> SqlPersistT m (Maybe (Key DbAoeTeam))
insertTeam = insertUnique

-- | TODO: wtf is up with not using the key here?
insertMap :: MonadIO m => DbMap -> SqlPersistT m (Maybe (Entity DbMap))
insertMap m@(DbMap mapName) = do
  _ <- insertUnique m
  getBy (DbMapUniqueName mapName)

-- |
entityToGameData :: Entity DbGame -> GameData
entityToGameData (Entity k DbGame{..}) = GameData (fromSqlKey k) $ GameDataBody {..}
    where
    gameName = dbGameName
    scenario_filename = dbGameScenario_filename
    num_players = dbGameNum_players
    duration = dbGameDuration
    allow_cheats = dbGameAllow_cheats
    complete = fromIntegral dbGameComplete
    map_size = fromIntegral dbGameMap_size
    map_id = fromIntegral dbGameRaw_map_id
    population = fromIntegral dbGamePopulation
    victory_type = fromIntegral dbGameVictory_type
    starting_age = fromIntegral dbGameStarting_age
    resources = fromIntegral dbGameResources
    all_techs = dbGameAll_techs
    team_together = dbGameTeam_together
    reveal_map = fromIntegral dbGameReveal_map
    is_death_match = dbGameIs_death_match
    is_regicide = dbGameIs_regicide
    lock_teams = dbGameLock_teams
    lock_speed = dbGameLock_speed
    game_length = mkTime' dbGameGame_length

-- |
gameDataBodyToDb :: GameDataBody -> Maybe (Key Db.DbMap) -> DbGame
gameDataBodyToDb GameDataBody {..} dbGameMapId = DbGame {..}
    where
    dbGameName = gameName
    dbGameScenario_filename = scenario_filename
    dbGameNum_players = num_players
    dbGameDuration = duration
    dbGameAllow_cheats = allow_cheats
    dbGameComplete = fromIntegral complete
    dbGameMap_size = fromIntegral map_size
    dbGameRaw_map_id = fromIntegral map_id
    dbGamePopulation = fromIntegral population
    dbGameVictory_type = fromIntegral victory_type
    dbGameStarting_age = fromIntegral starting_age
    dbGameResources = fromIntegral resources
    dbGameAll_techs = all_techs
    dbGameTeam_together = team_together
    dbGameReveal_map = fromIntegral reveal_map
    dbGameIs_death_match = is_death_match
    dbGameIs_regicide = is_regicide
    dbGameLock_teams = lock_teams
    dbGameLock_speed = lock_speed
    dbGameGame_length = unMkTime game_length

-- |
playerStatsToDb :: Db.Key DbGame -> Key DbPlayer -> Maybe (Key DbAoeTeam) -> PlayerStats -> DbPlayerStats
playerStatsToDb k pk tk PlayerStats{..} = DbPlayerStats{..}
    where
    dbPlayerStatsGameId = k
    dbPlayerStatsPlayer_id = pk
    dbPlayerStatsTeamId = tk
    dbPlayerStatsScore = score
    dbPlayerStatsVictory = victory
    dbPlayerStatsCivilization = toSqlKey . civId $ civilization
    dbPlayerStatsColor = fromIntegral color
    dbPlayerStatsTeam = fromIntegral team
    dbPlayerStatsAllies_count = fromIntegral allies_count
    dbPlayerStatsMvp = mvp
    dbPlayerStatsResult = fromIntegral result
    dbPlayerStatsMilitary_score = military_score
    dbPlayerStatsUnits_killed = units_killed
    dbPlayerStatsHp_killed = hp_killed
    dbPlayerStatsUnits_lost = units_lost
    dbPlayerStatsBuildings_razed = buildings_razed
    dbPlayerStatsHp_razed = hp_razed
    dbPlayerStatsBuildings_lost = buildings_lost
    dbPlayerStatsUnits_converted = units_converted
    dbPlayerStatsEconomy_score = economy_score
    dbPlayerStatsFood_collected = food_collected
    dbPlayerStatsWood_collected = wood_collected
    dbPlayerStatsStone_collected = stone_collected
    dbPlayerStatsGold_collected = gold_collected
    dbPlayerStatsTribute_sent = tribute_sent
    dbPlayerStatsTribute_received = tribute_received
    dbPlayerStatsTrade_profit = trade_profit
    dbPlayerStatsRelic_gold = relic_gold
    dbPlayerStatsTech_score = tech_score
    dbPlayerStatsFeudal_time = feudal_time
    dbPlayerStatsCastle_time = castle_time
    dbPlayerStatsImperial_time = imperial_time
    dbPlayerStatsMap_exploration = map_exploration
    dbPlayerStatsResearch_count = research_count
    dbPlayerStatsResearch_percent = research_percent
    dbPlayerStatsSociety_score = society_score
    dbPlayerStatsTotal_wonders = total_wonders
    dbPlayerStatsTotal_castles = total_castles
    dbPlayerStatsRelics_captured = relics_captured
    dbPlayerStatsVillager_high = villager_high
    dbPlayerStatsPlayer_units_killed1 = head player_units_killed
    dbPlayerStatsPlayer_units_killed2 = player_units_killed !! 1
    dbPlayerStatsPlayer_units_killed3 = player_units_killed !! 2
    dbPlayerStatsPlayer_units_killed4 = player_units_killed !! 3
    dbPlayerStatsPlayer_units_killed5 = player_units_killed !! 4
    dbPlayerStatsPlayer_units_killed6 = player_units_killed !! 5
    dbPlayerStatsPlayer_units_killed7 = player_units_killed !! 6
    dbPlayerStatsPlayer_buildings_razed1 = head player_buildings_razed
    dbPlayerStatsPlayer_buildings_razed2 = player_buildings_razed !! 1
    dbPlayerStatsPlayer_buildings_razed3 = player_buildings_razed !! 2
    dbPlayerStatsPlayer_buildings_razed4 = player_buildings_razed !! 3
    dbPlayerStatsPlayer_buildings_razed5 = player_buildings_razed !! 4
    dbPlayerStatsPlayer_buildings_razed6 = player_buildings_razed !! 5
    dbPlayerStatsPlayer_buildings_razed7 = player_buildings_razed !! 6
    dbPlayerStatsPlayer_tribute_sent1 = head player_tribute_sent
    dbPlayerStatsPlayer_tribute_sent2 = player_tribute_sent !! 1
    dbPlayerStatsPlayer_tribute_sent3 = player_tribute_sent !! 2
    dbPlayerStatsPlayer_tribute_sent4 = player_tribute_sent !! 3
    dbPlayerStatsPlayer_tribute_sent5 = player_tribute_sent !! 4
    dbPlayerStatsPlayer_tribute_sent6 = player_tribute_sent !! 5
    dbPlayerStatsPlayer_tribute_sent7 = player_tribute_sent !! 6

-- |
entityToPlayerStats :: Entity DbPlayerStats -> Entity DbCiv -> Text -> PlayerStats
entityToPlayerStats (Entity _ DbPlayerStats{..}) civ playerStatsPlayerName = PlayerStats {..}
    where
    score = dbPlayerStatsScore
    victory = dbPlayerStatsVictory
    civilization = entityToCiv civ
    color = fromIntegral dbPlayerStatsColor
    team = fromIntegral dbPlayerStatsTeam
    allies_count = fromIntegral dbPlayerStatsAllies_count
    mvp = dbPlayerStatsMvp
    result = fromIntegral dbPlayerStatsResult
    military_score = dbPlayerStatsMilitary_score
    units_killed = dbPlayerStatsUnits_killed
    hp_killed = dbPlayerStatsHp_killed
    units_lost = dbPlayerStatsUnits_lost
    buildings_razed = dbPlayerStatsBuildings_razed
    hp_razed = dbPlayerStatsHp_razed
    buildings_lost = dbPlayerStatsBuildings_lost
    units_converted = dbPlayerStatsUnits_converted
    economy_score = dbPlayerStatsEconomy_score
    food_collected = fromIntegral dbPlayerStatsFood_collected
    wood_collected = fromIntegral dbPlayerStatsWood_collected
    stone_collected = fromIntegral dbPlayerStatsStone_collected
    gold_collected = fromIntegral dbPlayerStatsGold_collected
    tribute_sent = dbPlayerStatsTribute_sent
    tribute_received = dbPlayerStatsTribute_received
    trade_profit = dbPlayerStatsTrade_profit
    relic_gold = dbPlayerStatsRelic_gold
    tech_score = dbPlayerStatsTech_score
    feudal_time = dbPlayerStatsFeudal_time
    castle_time = dbPlayerStatsCastle_time
    imperial_time = dbPlayerStatsImperial_time
    map_exploration = dbPlayerStatsMap_exploration
    research_count = dbPlayerStatsResearch_count
    research_percent = dbPlayerStatsResearch_percent
    society_score = dbPlayerStatsSociety_score
    total_wonders = dbPlayerStatsTotal_wonders
    total_castles = dbPlayerStatsTotal_castles
    relics_captured = dbPlayerStatsRelics_captured
    villager_high = dbPlayerStatsVillager_high
    player_units_killed = [dbPlayerStatsPlayer_units_killed1, dbPlayerStatsPlayer_units_killed2, dbPlayerStatsPlayer_units_killed3, dbPlayerStatsPlayer_units_killed4, dbPlayerStatsPlayer_units_killed5, dbPlayerStatsPlayer_units_killed6,dbPlayerStatsPlayer_units_killed7]
    player_buildings_razed = [dbPlayerStatsPlayer_buildings_razed1, dbPlayerStatsPlayer_buildings_razed2, dbPlayerStatsPlayer_buildings_razed3, dbPlayerStatsPlayer_buildings_razed4, dbPlayerStatsPlayer_buildings_razed5, dbPlayerStatsPlayer_buildings_razed6,dbPlayerStatsPlayer_buildings_razed7]
    player_tribute_sent = [dbPlayerStatsPlayer_tribute_sent1, dbPlayerStatsPlayer_tribute_sent2, dbPlayerStatsPlayer_tribute_sent3, dbPlayerStatsPlayer_tribute_sent4, dbPlayerStatsPlayer_tribute_sent5, dbPlayerStatsPlayer_tribute_sent6,dbPlayerStatsPlayer_tribute_sent7]

-- |
entityToCiv :: Entity DbCiv -> Civilization
entityToCiv (Entity k DbCiv{..}) = Civilization (fromSqlKey k) dbCivName

-- |
entityToGameMap :: Entity DbMap -> GameMap
entityToGameMap (Entity k DbMap{..}) = GameMap (fromSqlKey k) dbMapName

-- |
entityToTeam :: Entity DbAoeTeam -> AoeTeam
entityToTeam (Entity k DbAoeTeam{..}) = AoeTeam (fromSqlKey k) dbAoeTeamName

-- |
entityToPlayer :: Map (Key DbAoeTeam) AoeTeam -> Entity DbPlayer -> Player
entityToPlayer teamsMap (Entity pk DbPlayer{..}) = Player{..}
    where
    playerId   = fromSqlKey pk
    playerName = dbPlayerName
    playerTeam = flip Map.lookup teamsMap =<< dbPlayerTeamId

-- |
entityToTag :: Entity DbTag -> Tag
entityToTag (Entity k DbTag{..}) = Tag (fromSqlKey k) dbTagName
