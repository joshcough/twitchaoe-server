{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Stats.DatabaseModels where

import           Database.Persist.Postgresql.JSON ()
import           Database.Persist.TH              (mkDeleteCascade, mkPersist, persistLowerCase, share, sqlSettings)
import           Data.Text                        (Text)
import           Data.Word                        (Word8)

share [mkPersist sqlSettings, mkDeleteCascade sqlSettings] [persistLowerCase|
DbAoeTeam json sql=aoe_teams
    name                Text
    DbAoeTeamUniqueName name
    deriving Show Eq

DbPlayer json sql=players
    teamId             DbAoeTeamId Maybe
    name               Text
    DbPlayerUniqueName name
    deriving Show Eq

DbCiv json sql=civs
    name               Text
    deriving Show Eq

DbMap json sql=maps
    name               Text
    DbMapUniqueName    name
    deriving Show Eq

DbGame json sql=game_data
    mapId              DbMapId Maybe sql=map_id
    name               Text
    scenario_filename  Text
    num_players        Int
    duration           Int
    allow_cheats       Bool
    complete           Word8
    map_size           Word8
    raw_map_id         Word8 sql=raw_map_id
    population         Word8
    victory_type       Word8
    starting_age       Word8
    resources          Word8
    all_techs          Bool
    team_together      Bool
    reveal_map         Word8
    is_death_match     Bool
    is_regicide        Bool
    lock_teams         Bool
    lock_speed         Bool
    game_length        Int
    deriving Show Eq

DbPlayerStats json sql=game_player_stats
   player_id               DbPlayerId
   gameId                  DbGameId
   teamId                  DbAoeTeamId Maybe
   score                   Int
   victory                 Bool
   civilization            DbCivId
   color                   Word8
   team                    Word8
   allies_count            Word8
   mvp                     Bool
   result                  Word8
   military_score          Int
   units_killed            Int
   hp_killed               Int
   units_lost              Int
   buildings_razed         Int
   hp_razed                Int
   buildings_lost          Int
   units_converted         Int
   player_units_killed1    Int
   player_units_killed2    Int
   player_units_killed3    Int
   player_units_killed4    Int
   player_units_killed5    Int
   player_units_killed6    Int
   player_units_killed7    Int
   player_buildings_razed1 Int
   player_buildings_razed2 Int
   player_buildings_razed3 Int
   player_buildings_razed4 Int
   player_buildings_razed5 Int
   player_buildings_razed6 Int
   player_buildings_razed7 Int
   economy_score           Int
   food_collected          Int
   wood_collected          Int
   stone_collected         Int
   gold_collected          Int
   tribute_sent            Int
   tribute_received        Int
   trade_profit            Int
   relic_gold              Int
   player_tribute_sent1    Int
   player_tribute_sent2    Int
   player_tribute_sent3    Int
   player_tribute_sent4    Int
   player_tribute_sent5    Int
   player_tribute_sent6    Int
   player_tribute_sent7    Int
   tech_score              Int
   feudal_time             Int
   castle_time             Int
   imperial_time           Int
   map_exploration         Int
   research_count          Int
   research_percent        Int
   society_score           Int
   total_wonders           Int
   total_castles           Int
   relics_captured         Int
   villager_high           Int
   deriving Show Eq

DbTag json sql=tags
    name            Text
    DbTagUniqueName name
    deriving Show Eq

DbGameTag json sql=game_tags
    gameId DbGameId
    tagId  DbTagId
    deriving Show Eq
|]
