
module Stats.PSBridge where

import Data.Proxy
import Language.PureScript.Bridge
import Language.PureScript.Bridge.SumType (equal, order)
import Stats.Models
import Stats.Aggregations
import Stats.Html

main :: IO ()
main = writePSTypes "frontend/src" (buildBridge defaultBridge) myTypes

-- TODO: there are more types in Models that maybe should be added here
myTypes :: [SumType 'Haskell]
myTypes = [
    let p = (Proxy :: Proxy AggregatedPlayerStats)         in e p
  , let p = (Proxy :: Proxy AggregatedMapStats)            in e p
  , let p = (Proxy :: Proxy AggregatedCivStats)            in e p
  , let p = (Proxy :: Proxy AggregatedTeamStats)           in e p
  , let p = (Proxy :: Proxy AggregatedStatsGroup)          in e p
  , let p = (Proxy :: Proxy AggregatedMapStatsGroup)       in e p
  , let p = (Proxy :: Proxy AggregatedCommonStats)         in e p
  , let p = (Proxy :: Proxy AggregatedStats)               in e p
  , let p = (Proxy :: Proxy GameData)                      in e p
  , let p = (Proxy :: Proxy GameDataBody)                  in e p
  , let p = (Proxy :: Proxy GameImportResult)              in e p
  , let p = (Proxy :: Proxy Player)                        in o p
  , let p = (Proxy :: Proxy GameMap)                       in o p
  , let p = (Proxy :: Proxy PlayerStats)                   in e p
  , let p = (Proxy :: Proxy AoeTeam)                       in o p
  , let p = (Proxy :: Proxy Civilization)                  in o p
  , let p = (Proxy :: Proxy Time)                          in o p
  , let p = (Proxy :: Proxy Uptimes)                       in o p
  , let p = (Proxy :: Proxy AggregatedPlayerStatsResponse) in e p
  , let p = (Proxy :: Proxy AggregatedTeamStatsResponse)   in e p
  , let p = (Proxy :: Proxy AggregatedCivStatsResponse)    in e p
  , let p = (Proxy :: Proxy AggregatedMapStatsResponse)    in e p
  ]
  where
  e p = equal p (mkSumType p)
  o p = order p (mkSumType p)
