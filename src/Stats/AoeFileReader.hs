
module Stats.AoeFileReader (
    main
  , main'
  , readGameFile
  , readSplitGameFiles
  , processZipFile
  , getRecursiveContents) where

import           Codec.Archive.Zip              (withArchive, unpackInto)
import           Control.Monad                  (replicateM, when)
import           Control.Monad.Except           (MonadIO, liftIO)
import           Control.Monad.State.Strict     (StateT, get, modify', evalStateT)
import           Control.Monad.Trans.Class      (lift)
import           Data.Aeson.Encode.Pretty       (encodePretty)
import           Data.Binary.Get                (Get, getByteString, getRemainingLazyByteString, getFloatle, getInt32le,
                                                 getWord16le, getWord32le, getWord8, isEmpty, runGet, skip)
import qualified Data.ByteString.Lazy        as BSL hiding (putStrLn)
import qualified Data.ByteString.Lazy.Char8  as BSL
import           Data.Int                       (Int32)
import           Data.List                      (sortBy)
import           Data.List.NonEmpty             (NonEmpty)
import qualified Data.List.NonEmpty          as NE
import           Data.Monoid                    ((<>))
import           Data.String.Utils              (endswith)
import           Data.Text                      (Text, replace, strip, pack, unpack)
import           Data.Text.Encoding             (decodeUtf8)
import           Data.Word                      (Word8, Word16)
import           System.Directory               (doesDirectoryExist, removeDirectoryRecursive)
import           System.Environment             (getArgs)
import           System.FilePath.Posix          (takeBaseName)

import           Stats.Models                   (GameImportResult(..), GameDataBody(..), PlayerStats(..), mkTime', civById', getMapNameFromGameName)
import           Util.Utils                     (getRecursiveContents) --, tShow)

main :: IO ()
main = do
    [f] <- getArgs
    main' f

main' :: FilePath -> IO ()
main' fp = do
  mRes <- readGameFile (pack  fp) fp
  maybe (print ("didn't find the stats!" :: String)) (BSL.putStrLn . encodePretty) mRes

-- | process a single recording file
readGameFile :: MonadIO m => Text -> FilePath -> m (Maybe GameImportResult)
readGameFile gameName file = liftIO $ runGet (readAOEFile gameName) <$> BSL.readFile file

-- | process a split recording file
readSplitGameFiles :: MonadIO m => Text -> FilePath -> NonEmpty FilePath -> m (Maybe GameImportResult)
readSplitGameFiles gameName part1 rest = combineResults <$> traverse (readGameFile gameName) (part1 : NE.toList rest)
    where
    combineResults :: [Maybe GameImportResult] -> Maybe GameImportResult
    combineResults = foldr f Nothing
        where f Nothing Nothing = Nothing
              f Nothing x = x
              f x Nothing = x
              f (Just g1) (Just g2) = Just $ g2 {
                playerStats = zipWith takeLeftCiv (playerStatsSorted g1) (playerStatsSorted g2)
              }
              takeLeftCiv p1 p2 =
                if playerStatsPlayerName p1 /= playerStatsPlayerName p2
                then error . unpack $ "not same player! " <> playerStatsPlayerName p1 <> ", " <> playerStatsPlayerName p2
                else p2 { civilization = civilization p1 }
              playerStatsSorted = sortBy (\ps1 ps2 -> compare (playerStatsPlayerName ps1) (playerStatsPlayerName ps2)) . playerStats

-- | process a zip file of recordings
processZipFile :: (MonadIO m) => Text -> FilePath -> m [(FilePath, Maybe GameImportResult)]
processZipFile filename path = do
    -- $(logDebug) $ pack $ "Processing zip file: " <> unpack filename <> ", " <> path
    let dir = "/tmp/" <> takeBaseName (unpack filename)
    liftIO $ do e <- doesDirectoryExist dir
                when e $ removeDirectoryRecursive dir
    withArchive path (unpackInto "/tmp")
    res <- processFileOrDir dir
    liftIO $ removeDirectoryRecursive dir
    return res

-- | process a whole directory of recordings
processFileOrDir :: (MonadIO m) => FilePath -> m [(FilePath, Maybe GameImportResult)]
processFileOrDir dir = do
    recordings <- mgzFiles dir
    -- $(logDebug) $ pack $ "Recordings in zip file: : " <> show recordings
    traverse go recordings
    where
    go f' = do -- $(logDebug) $ pack $ "Starting reading on: " <> takeBaseName f'
               r <- readGameFile (pack $ takeBaseName f') f'
               return (f', r)

    mgzFiles :: MonadIO m => FilePath -> m [FilePath]
    mgzFiles = fmap (filter $ endswith ".mgz") . getRecursiveContents

data Command = Command GameCommand | Meta MetaCommand | Sync Int32 | Tick Int32
    deriving (Eq, Show)

data MetaCommand = GameStart | Chat Text
    deriving (Eq, Show)

data GameCommand = InGameCommand InGameCommand | PostGameCommand GameDataBody [PlayerStats]
    deriving (Eq, Show)

data InGameCommand = TrainUnit Int32 Word16 Word16 | BuildBuilding Word8 Word16 | OtherInGameCommand
    deriving (Eq, Show)

-- | Top level Get function to read the file
readAOEFile :: Text -> Get (Maybe GameImportResult)
readAOEFile gameName = evalStateT action 0
    where

    action = do
        lift getHeader
        pluckGameStats <$> getCommands

        where
        pluckGameStats :: [Command] -> Maybe GameImportResult
        pluckGameStats (Command (PostGameCommand gd ps) : _) =
            Just (GameImportResult gd (getMapNameFromGameName gameName) ps)
        pluckGameStats (_:cs) = pluckGameStats cs
        pluckGameStats [] = Nothing

    -- |
    getHeader :: Get ()
    getHeader = do
         bodyStartLocation <- getInt32le
         _t <- getInt32le -- this is some header info called `t` in the python code...
         skip $ fromIntegral bodyStartLocation - 8

    -- |
    getCommands :: StateT Int32 Get [Command]
    getCommands = lift isEmpty >>= \mt -> if mt then return [] else (:) <$> getCommand <*> getCommands

    -- |
    getCommand :: StateT Int32 Get Command
    getCommand = do
        operation <- lift getInt32le
        case operation of
            1 -> Command <$> getGameCommand
            2 -> do t <- lift readSync
                    modify' (t+)
                    return $ Sync t
            3 -> lift $ Meta <$> getMetaCommand
            4 -> lift $ Meta <$> getMetaCommand
            x -> return $ Tick x

    -- |
    readSync :: Get Int32
    readSync = do
        time <- getInt32le
        temp <- getInt32le
        when (temp == 0) (skip 28)
        skip 12
        return time

    -- |
    getMetaCommand :: Get MetaCommand
    getMetaCommand = getInt32le >>= \case
        500  -> skip 20 >> return GameStart  -- _META_GAME_START = 500
        (-1) -> do                           -- _META_CHAT = -1
            len <- getInt32le
            msg <- getText $ fromIntegral len
            return $ Chat msg
            -- TODO: there's some stuff in the python code to read the player id from the message. could be useful.
        x -> fail $ "couldn't read MetaCommand from: : " <> show x

    -- |
    getGameCommand :: StateT Int32 Get GameCommand
    getGameCommand = do
        len     <- lift getInt32le
        command <- lift getWord8
        let postGame = 255
        if command /= postGame
        then lift $ getInGameCommand len command
        else getPostGameCommand

    -- |
    getInGameCommand :: Int32 -> Word8 -> Get GameCommand
    getInGameCommand len commandId = InGameCommand <$> case commandId of
        102 -> do -- _COMMAND_BUILD = 102
            selectionCount <- getWord8
            playerId       <- getWord8
            skip 1
            _coor          <- skip 4 >> skip 4
            buildingType   <- getWord16le
            skip 6
            _spriteId      <- skip 4
            skip (fromIntegral selectionCount * 4)
            return $ BuildBuilding playerId buildingType
        119 -> do -- _COMMAND_TRAIN = 119
            skip 3
            buildingId <- getInt32le
            unitType   <- getWord16le
            amount     <- getWord16le
            return $ TrainUnit buildingId unitType amount
        _   -> skip (fromIntegral len - 1) >> return OtherInGameCommand

    -- |
    getPostGameCommand :: StateT Int32 Get GameCommand
    getPostGameCommand = do
        time <- get
        lift $ do
            gd <- getGameDataBody time
            players <- replicateM (fromIntegral $ num_players gd) getPlayerStats
            -- consume the rest of the bytes because we are done.
            _ <- getRemainingLazyByteString
            return $ PostGameCommand gd players

    -- |
    getGameDataBody :: Int32 -> Get GameDataBody
    getGameDataBody game_length_ms = do
        skip 3
        scenario_filename <- getText 32
        num_players       <- fromIntegral <$> getInt32le
        duration          <- fromIntegral <$> getInt32le
        allow_cheats      <- getBool "allow_cheats"
        complete          <- fromIntegral <$> getWord8
        skip 10
        _unknown0         <- getFloatle
        map_size          <- fromIntegral <$> getWord8
        map_id            <- fromIntegral <$> getWord8
        population        <- fromIntegral <$> getWord8
        skip 1
        victory_type      <- fromIntegral <$> getWord8
        starting_age      <- fromIntegral <$> getWord8
        resources         <- fromIntegral <$> getWord8
        all_techs         <- getBool "all_techs"
        team_together     <- getBool "team_together"
        reveal_map        <- fromIntegral <$> getWord8
        is_death_match    <- getBool "is_death_match"
        is_regicide       <- getBool "is_regicide"
        _unknown1         <- getWord8
        lock_teams        <- getBool "lock_teams"
        lock_speed        <- getBool "lock_speed"
        _unknown2         <- getWord8
        let game_length = mkTime' . fromIntegral $ game_length_ms `div` 1000
        return GameDataBody {..}

    -- |
    getPlayerStats :: Get PlayerStats
    getPlayerStats = do
        -- $(logDebug) "getting player stats"
        playerStatsPlayerName  <- getText 16
        -- $(logDebug) $ "player name: " <> playerStatsPlayerName
        score                  <- fromIntegral <$> getWord16le
        -- $(logDebug) $ "score: " <> tShow score
        skip 16
        victory                <- getBool "victory"
        -- $(logDebug)  $ "victory: " <> tShow victory
        civId <- getWord8
        -- $(logDebug)  $ "civId: " <> tShow civId
        let civilization       = civById' . fromIntegral $ civId
        color                  <- fromIntegral <$> getWord8
        team                   <- fromIntegral <$> getWord8
        -- $(logDebug)  $ "team: " <> tShow team
        allies_count           <- fromIntegral <$> getWord8
        -- $(logDebug)  $ "allies_count: " <> tShow allies_count
        skip 1
        mvp                    <- getBool "mvp"
        -- $(logDebug)  $ "mvp: " <> tShow mvp
        skip 3
        result                 <- fromIntegral <$> getWord8
        -- $(logDebug)  $ "result: " <> tShow result
        skip 3
        military_score         <- fromIntegral <$> getWord16le
        -- $(logDebug)  $ "military_score: " <> tShow military_score
        units_killed           <- fromIntegral <$> getWord16le
        -- $(logDebug)  $ "units_killed: " <> tShow units_killed
        hp_killed              <- fromIntegral <$> getWord16le
        -- $(logDebug)  $ "hp_killed: " <> tShow hp_killed
        units_lost             <- fromIntegral <$> getWord16le
        -- $(logDebug)  $ "units_lost: " <> tShow units_lost
        buildings_razed        <- fromIntegral <$> getWord16le
        -- $(logDebug)  $ "buildings_razed: " <> tShow buildings_razed
        hp_razed               <- fromIntegral <$> getWord16le
        -- $(logDebug)  $ "hp_razed: " <> tShow hp_razed
        buildings_lost         <- fromIntegral <$> getWord16le
        -- $(logDebug)  $ "buildings_lost: " <> tShow buildings_lost
        units_converted        <- fromIntegral <$> getWord16le
        -- $(logDebug)  $ "units_converted: " <> tShow units_converted
        _unknown1              <- getWord16le
        player_units_killed    <- replicateM 7 $ fromIntegral <$> getWord16le
        player_buildings_razed <- replicateM 7 $ fromIntegral <$> getWord16le
        skip 2
        economy_score          <- fromIntegral <$> getWord16le
        skip 2
        food_collected         <- fromIntegral <$> getWord32le
        wood_collected         <- fromIntegral <$> getWord32le
        stone_collected        <- fromIntegral <$> getWord32le
        gold_collected         <- fromIntegral <$> getWord32le
        tribute_sent           <- fromIntegral <$> getWord16le
        tribute_received       <- fromIntegral <$> getWord16le
        trade_profit           <- fromIntegral <$> getWord16le
        relic_gold             <- fromIntegral <$> getWord16le
        player_tribute_sent    <- replicateM 7 $ fromIntegral <$> getWord16le
        skip 2
        tech_score             <- fromIntegral <$> getWord16le
        skip 2
        feudal_time            <- fromIntegral <$> getInt32le
        castle_time            <- fromIntegral <$> getInt32le
        imperial_time          <- fromIntegral <$> getInt32le
        map_exploration        <- fromIntegral <$> getWord8
        research_count         <- fromIntegral <$> getWord8
        research_percent       <- fromIntegral <$> getWord8
        society_score          <- fromIntegral <$> getWord16le
        total_wonders          <- fromIntegral <$> getWord8
        total_castles          <- fromIntegral <$> getWord8
        relics_captured        <- fromIntegral <$> getWord8
        _unknown2              <- getWord8
        _unknown3              <- getWord8
        villager_high          <- fromIntegral <$> getWord16le
        -- $(logDebug) $ "villager_high: " <> tShow villager_high
        skip 84
        return PlayerStats {..}

    -- |
    getText :: Int -> Get Text
    getText n = replace "\x00" "" . strip . decodeUtf8 <$> getByteString n

    -- |
    getBool :: Text -> Get Bool
    getBool context = getWord8 >>= \case
        1 -> return True
        0 -> return False
        i -> fail $ "When trying to read: " <> unpack context <> ", couldn't read bool from: : " <> show i
