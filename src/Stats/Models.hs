{-# LANGUAGE DeriveAnyClass    #-}

module Stats.Models (
    Civilization(..)
  , AoeTeam(..)
  , Player(..)
  , Game(..)
  , GameData(..)
  , GameDataBody(..)
  , GameMap(..)
  , GameImportResult(..)
  , PlayerStats(..)
  , Tag(..)
  , Time(..), Uptimes(..), mkTime, mkTime', unMkTime, mkUptimes
  , civList, civsById, civById, civById', civByName, civsByName
  , getMapNameFromGameName
  ) where

import           Data.Aeson                     (FromJSON, ToJSON)
import           Data.Int                       (Int64)
import           Data.Map                       (Map)
import qualified Data.Map                       as Map
import           Data.Maybe                     (fromMaybe)
import           Data.Text                      (Text, pack, toLower, unpack)
import           GHC.Generics                   (Generic)
import           System.FilePath.Posix          (takeBaseName)

-- |
data Civilization = Civilization {
    civId   :: Int64
  , civName :: Text
} deriving (Eq, Ord, Show, Generic, ToJSON, FromJSON)

-- |
data AoeTeam = AoeTeam {
    teamId   :: Int64
  , teamName :: Text
  } deriving (Eq, Ord, Show, Generic, ToJSON, FromJSON)

-- |
data GameMap = GameMap {
    mapId   :: Int64
  , mapName :: Text
  } deriving (Eq, Ord, Show, Generic, ToJSON, FromJSON)

-- |
data Player = Player {
    playerId   :: Int64
  , playerName :: Text
  , playerTeam :: Maybe AoeTeam
  } deriving (Eq, Ord, Show, Generic, ToJSON, FromJSON)

-- |
data GameImportResult = GameImportResult {
    gameData    :: GameDataBody
  , gameMap     :: Maybe Text
  , playerStats :: [PlayerStats]
  } deriving (Eq, Show, Generic, ToJSON, FromJSON)

-- |
data Game = Game {
    gameGameData    :: GameData
  , gameDataGameMap :: GameMap
  , gamePlayerStats :: [(Player, Civilization, PlayerStats)]
  , gameTags        :: [Tag]
  } deriving (Eq, Show, Generic, ToJSON, FromJSON)

-- |
data Tag = Tag {
    tagId   :: Int64
  , tagText :: Text
  } deriving (Eq, Show, Generic, ToJSON, FromJSON)

-- |
data GameData = GameData {
    gameDataId   :: Int64
  , gameDataBody :: GameDataBody
  } deriving (Eq, Show, Generic, ToJSON, FromJSON)

-- |
data GameDataBody = GameDataBody {
     gameName          :: Text
  ,  scenario_filename :: Text
  ,  num_players       :: Int
  ,  duration          :: Int
  ,  allow_cheats      :: Bool
  ,  complete          :: Int
  ,  map_size          :: Int
  ,  map_id            :: Int
  ,  population        :: Int
  ,  victory_type      :: Int
  ,  starting_age      :: Int
  ,  resources         :: Int
  ,  all_techs         :: Bool
  ,  team_together     :: Bool
  ,  reveal_map        :: Int
  ,  is_death_match    :: Bool
  ,  is_regicide       :: Bool
  ,  lock_teams        :: Bool
  ,  lock_speed        :: Bool
  ,  game_length       :: Time
  } deriving (Eq, Show, Generic, ToJSON, FromJSON)

-- |
data PlayerStats = PlayerStats {
    playerStatsPlayerName  :: Text
  , score                  :: Int
  , victory                :: Bool
  , civilization           :: Civilization
  , color                  :: Int
  , team                   :: Int
  , allies_count           :: Int
  , mvp                    :: Bool
  , result                 :: Int
  , military_score         :: Int
  , units_killed           :: Int
  , hp_killed              :: Int
  , units_lost             :: Int
  , buildings_razed        :: Int
  , hp_razed               :: Int
  , buildings_lost         :: Int
  , units_converted        :: Int
  , player_units_killed    :: [Int]
  , player_buildings_razed :: [Int]
  , economy_score          :: Int
  , food_collected         :: Int
  , wood_collected         :: Int
  , stone_collected        :: Int
  , gold_collected         :: Int
  , tribute_sent           :: Int
  , tribute_received       :: Int
  , trade_profit           :: Int
  , relic_gold             :: Int
  , player_tribute_sent    :: [Int]
  , tech_score             :: Int
  , feudal_time            :: Int
  , castle_time            :: Int
  , imperial_time          :: Int
  , map_exploration        :: Int
  , research_count         :: Int
  , research_percent       :: Int
  , society_score          :: Int
  , total_wonders          :: Int
  , total_castles          :: Int
  , relics_captured        :: Int
  , villager_high          :: Int
  } deriving (Eq, Show, Generic, ToJSON, FromJSON)

-- |
data Time = Time {
    hours   :: Int
  , minutes :: Int
  , seconds :: Int
  } deriving (Eq, Ord, Show, Generic, ToJSON, FromJSON)

instance Num Time where
    t1 + t2 = mkTime' $ unMkTime t1 + unMkTime t2
    t1 - t2 = mkTime' $ unMkTime t1 - unMkTime t2
    t1 * t2 = mkTime' $ unMkTime t1 * unMkTime t2
    abs    (Time h m s) = Time (abs h)    (abs m)    (abs s)
    signum (Time h m s) = Time (signum h) (signum m) (signum s)
    fromInteger = mkTime' . fromIntegral

-- |
data Uptimes = Uptimes {
    uptimeFeudal   :: Maybe Time
  , uptimeCastle   :: Maybe Time
  , uptimeImperial :: Maybe Time
  } deriving (Eq, Ord, Show, Generic, ToJSON, FromJSON)

-- |
mkTime :: Int -> Maybe Time
mkTime t = if t < 0 then Nothing else Just $ mkTime' t

-- |
mkTime' :: Int -> Time
mkTime' t = Time {..}
    where seconds = t `mod` 60
          minutes = t `div` 60 `mod` 60
          hours   = t `div` 3600

-- |
unMkTime :: Time -> Int
unMkTime (Time h m s) = (h * 3600) + (m * 60) + s

mkUptimes :: Maybe Int -> Maybe Int -> Maybe Int -> Uptimes
mkUptimes f c i = Uptimes (f >>= mkTime) (c >>= mkTime) (i >>= mkTime)

-- |
civList :: [Civilization]
civList = Map.elems civsById

-- |
civsById :: Map Int64 Civilization
civsById = Map.fromList $ byId <$> civs where byId (i, c) = (i, Civilization i c)

-- |
civsByName :: Map Text Civilization
civsByName = Map.fromList $ byName <$> civs where byName (i, c) = (c, Civilization i c)

-- |
civs :: [(Int64, Text)]
civs = [
    (-1, "Unknown")
  , (0,  "Gaia") -- screw gaia for now, maybe forever.
  , (1,  "Britons")
  , (2,  "Franks")
  , (3,  "Goths")
  , (4,  "Teutons")
  , (5,  "Japanese")
  , (6,  "Chinese")
  , (7,  "Byzantines")
  , (8,  "Persians")
  , (9,  "Saracens")
  , (10, "Turks")
  , (11, "Vikings")
  , (12, "Mongols")
  , (13, "Celts")
  , (14, "Spanish")
  , (15, "Aztecs")
  , (16, "Mayans")
  , (17, "Huns")
  , (18, "Koreans")
  , (19, "Italians")
  , (20, "Indians")
  , (21, "Incas")
  , (22, "Magyars")
  , (23, "Slavs")
  , (24, "Portuguese")
  , (25, "Ethiopians")
  , (26, "Malians")
  , (27, "Berbers")
  , (28, "Khmer")
  , (29, "Malay")
  , (30, "Burmese")
  , (31, "Vietnamese")
  ]

unknownCiv :: Civilization
unknownCiv = Civilization (-1) "Unknown"

-- |
civById' :: Int64 -> Civilization
civById' i = fromMaybe unknownCiv $ Map.lookup i civsById

-- |
civById :: Int64 -> Maybe Civilization
civById i = Map.lookup i civsById

-- |
civByName :: Text -> Maybe Civilization
civByName t = Map.lookup t civsByName

-- |
mapsList :: [Text]
mapsList = [
    "Acropolis"
  , "Arabia"
  , "Arena"
  , "Baltic"
  , "Budapest"
  , "Cross"
  , "Scandinavia"
  , "Valley"
  , "Serengeti"
  , "Desert"
  , "Gorge"
  , "Sahara"
  , "Team_Islands"
  ]

-- |
mapsMap :: Map Text Text
mapsMap = Map.fromList $ (\m -> (m, m)) . toLower <$> mapsList

-- |
getMapNameFromGameName :: Text -> Maybe Text
getMapNameFromGameName = flip Map.lookup mapsMap . pack . last . words . takeBaseName . unpack . toLower

