{-# LANGUAGE DeriveAnyClass    #-}

module Stats.Aggregations (
    AggregationsDb(..)
  ---
  , AggregatedStats(..)
  , AggregatedCivStats(..)
  , AggregatedCommonStats(..)
  , AggregatedStatsGroup(..)
  , AggregatedMapStats(..)
  , AggregatedMapStatsGroup(..)
  , AggregatedPlayerStats(..)
  , AggregatedTeamStats(..)
  ---
  ) where

import           Control.Monad.Except       (MonadIO)
import           Data.Aeson                 (FromJSON, ToJSON)
import qualified Data.Map                   as Map
import           Data.Tuple.Utils           (thd3)
import           Database.Esqueleto         (SqlPersistT)
import           GHC.Generics               (Generic)

import           Config                     (AppT', runDb)
import           Stats.GameViews            (isTeamGame, viewPlayer, viewTeam, viewMap, viewCiv, viewTeamGames, view1v1s)
import           Stats.Models               ( Civilization(..), Game(..), GameData(..), GameDataBody(..), GameMap(..), PlayerStats(..)
                                            , AoeTeam(..), Player(..), Time(..), mkTime, mkTime', unMkTime, Uptimes(..))
import           Stats.Storage              ( getDbStatsByCiv, getDbStatsByMap, getDbStatsByPlayer, getDbStatsByTeam
                                            , getDbStatsForAllCivs, getDbStatsForAllMaps, getDbStatsForAllPlayers, getDbStatsForAllTeams)
import           Util.Utils                 (average)

class Monad m => AggregationsDb m where
    getStatsByCiv :: Civilization -> m AggregatedCivStats
    getStatsByMap :: GameMap -> m AggregatedMapStatsGroup
    getStatsByPlayer :: Player -> m AggregatedPlayerStats
    getStatsByTeam :: AoeTeam -> m AggregatedTeamStats
    getStatsForPlayers :: m [AggregatedPlayerStats]
    getStatsForTeams :: m [AggregatedTeamStats]
    getStatsForCivs :: m [AggregatedCivStats]
    getStatsForMaps :: m [AggregatedMapStatsGroup]

instance MonadIO m => AggregationsDb (AppT' e m) where
    getStatsByCiv = runDb . getStatsByCiv
    getStatsByMap = runDb . getStatsByMap
    getStatsByPlayer = runDb . getStatsByPlayer
    getStatsByTeam = runDb . getStatsByTeam
    getStatsForPlayers = runDb getStatsForPlayers
    getStatsForTeams = runDb getStatsForTeams
    getStatsForCivs = runDb getStatsForCivs
    getStatsForMaps = runDb getStatsForMaps

instance MonadIO m => AggregationsDb (SqlPersistT m) where
    getStatsByCiv c = AggregatedCivStats c . aggregateStatsGroup . viewCiv c <$> getDbStatsByCiv c
    getStatsByMap m = fmap (aggregateMapStatsGroup m . viewMap m) . getDbStatsByMap $ m
    getStatsByPlayer p = AggregatedPlayerStats p . aggregateStatsGroup . viewPlayer p <$> getDbStatsByPlayer p
    getStatsByTeam t = AggregatedTeamStats t . aggregateStatsGroup . viewTeam t <$> getDbStatsByTeam t
    getStatsForPlayers = fmap f . Map.toList <$> getDbStatsForAllPlayers
        where f (p, games) = AggregatedPlayerStats p $ aggregateStatsGroup (viewPlayer p games)
    getStatsForTeams = fmap f . Map.toList <$> getDbStatsForAllTeams
        where f (t, games) = AggregatedTeamStats t $ aggregateStatsGroup (viewTeam t games)
    getStatsForCivs = fmap f . Map.toList <$> getDbStatsForAllCivs
        where f (c, games) = AggregatedCivStats c $ aggregateStatsGroup (viewCiv c games)
    getStatsForMaps = fmap f . Map.toList <$> getDbStatsForAllMaps
        where f (m, games) = aggregateMapStatsGroup m (viewMap m games)

-- |
data AggregatedPlayerStats = AggregatedPlayerStats {
    playerStatsPlayer :: Player
  , playerStatsAgg    :: AggregatedStatsGroup
} deriving (Eq, Show, Generic, ToJSON, FromJSON)

-- |
data AggregatedTeamStats = AggregatedTeamStats {
    teamStatsTeam :: AoeTeam
  , teamStatsAgg  :: AggregatedStatsGroup
} deriving (Eq, Show, Generic, ToJSON, FromJSON)

-- |
data AggregatedCivStats = AggregatedCivStats {
    civStatsCiv :: Civilization
  , civStatsAgg  :: AggregatedStatsGroup
} deriving (Eq, Show, Generic, ToJSON, FromJSON)

-- |
data AggregatedStatsGroup = AggregatedStatsGroup {
    aggStatsTotals :: Maybe AggregatedStats
  , aggStats1v1s   :: Maybe AggregatedStats
  , aggStatsTgs    :: Maybe AggregatedStats
} deriving (Eq, Show, Generic, ToJSON, FromJSON)

-- |
data AggregatedMapStatsGroup = AggregatedMapStatsGroup {
    aggMapStatsTotals :: Maybe AggregatedMapStats
  , aggMapStats1v1s   :: Maybe AggregatedMapStats
  , aggMapStatsTgs    :: Maybe AggregatedMapStats
} deriving (Eq, Show, Generic, ToJSON, FromJSON)

{-
TODO: add these:
Towers Built   ---- cant do now
Resources Sold ---- cant do now
-}
-- |
data AggregatedStats = AggregatedStats {
    statsNumWins     :: Int
  , statsNumLosses   :: Int
  , statsWinPct      :: Double
  , statsNumMVPs     :: Int
  , statsKDRatio     :: Double
  , statsCommonStats :: AggregatedCommonStats
} deriving (Eq, Show, Generic, ToJSON, FromJSON)

-- |
data AggregatedMapStats = AggregatedMapStats {
    mapStatsMap         :: GameMap
  , mapStatsCommonStats :: AggregatedCommonStats
} deriving (Eq, Show, Generic, ToJSON, FromJSON)

data AggregatedCommonStats = AggregatedCommonStats {
    statsGamesPlayed     :: Int
  , statsHPDamage        :: Double
  , statsNumKilled       :: Double
  , statsAvgFood         :: Double
  , statsAvgWood         :: Double
  , statsAvgGold         :: Double
  , statsAvgStone        :: Double
  , statsTributeSent     :: Double
  , statsTributeReceived :: Double
  , statsAvgMapExp       :: Double
  , statsRelicGold       :: Double
  , statsAvgGameLength   :: Time
  , statsAvgUptimes      :: Uptimes
} deriving (Eq, Show, Generic, ToJSON, FromJSON)

-- |
aggregateStatsGroup :: [Game] -> AggregatedStatsGroup
aggregateStatsGroup games = AggregatedStatsGroup {..}
    where
    aggStatsTotals = aggregateStats games
    aggStats1v1s   = aggregateStats $ view1v1s games
    aggStatsTgs    = aggregateStats $ viewTeamGames games

-- |
aggregateMapStatsGroup :: GameMap -> [Game] -> AggregatedMapStatsGroup
aggregateMapStatsGroup m games = AggregatedMapStatsGroup {..}
    where
    aggMapStatsTotals = aggregateMapStats m games
    aggMapStats1v1s   = aggregateMapStats m $ view1v1s games
    aggMapStatsTgs    = aggregateMapStats m $ viewTeamGames games

-- | TODO: make stats a monoid to combine each field
--   TODO: this could/should be done with a big fold instead of making many passes over the data. but for now, its fine.
aggregateStats :: [Game] -> Maybe AggregatedStats
aggregateStats [] = Nothing
aggregateStats games = Just $ AggregatedStats {..}
    where
    allRows = games >>= gameStats
    nrGames = length games
    -----
    statsNumWins     = sum $ nrWinsInGame <$> games
    statsNumLosses   = nrGames - statsNumWins
    statsWinPct      = winPct statsNumWins statsNumLosses nrGames
    statsNumMVPs     = countInStats allRows mvp
    statsKDRatio     = avgInStats allRows units_killed / avgInStats allRows units_lost
    statsCommonStats = aggregateCommonStats games

aggregateCommonStats :: [Game] -> AggregatedCommonStats
aggregateCommonStats games = AggregatedCommonStats {..}
    where
    allRows = games >>= gameStats
    tgRows  = filter isTeamGame games >>= gameStats
    -----
    statsGamesPlayed     = length games
    statsHPDamage        = avgInStats allRows hp_killed
    statsNumKilled       = avgInStats allRows units_killed
    statsAvgFood         = avgInStats allRows food_collected
    statsAvgWood         = avgInStats allRows wood_collected
    statsAvgGold         = avgInStats allRows gold_collected
    statsAvgStone        = avgInStats allRows stone_collected
    statsTributeSent     = avgInStats tgRows  tribute_sent
    statsTributeReceived = avgInStats tgRows  tribute_received
    statsAvgMapExp       = avgInStats allRows map_exploration
    statsRelicGold       = avgInStats allRows relic_gold
    statsAvgGameLength   = avgTime $ game_length . gameDataBody . gameGameData <$> games
    statsAvgUptimes      = avgUptimesForRows allRows

--     TODO: get most played civ and map for Tati!

-- |
aggregateMapStats :: GameMap -> [Game] -> Maybe AggregatedMapStats
aggregateMapStats _ [] = Nothing
aggregateMapStats m games = Just . AggregatedMapStats m $ aggregateCommonStats games

-- |
avgInStats :: [a] -> (a -> Int) -> Double
avgInStats rows f = average $ f <$> rows

-- |
countInStats :: [PlayerStats] -> (PlayerStats -> Bool) -> Int
countInStats rows f = length $ filter f rows

-- | Note: this calc doesn't really work in general because its assuming a view over the games.
--   so, don't ever stick it in models. keep it with the aggregation code.
nrWinsInGame :: Game -> Int
nrWinsInGame g = f
    where wins = countInStats (gameStats g) victory
          f | isTeamGame g = if wins > 0 then 1 else 0
            | otherwise = wins -- this will be either 1 or 0.

-- |
winPct :: (Eq a3, Num a3, Integral a2, Integral a1) => a1 -> a3 -> a2 -> Double
winPct wins losses total = if losses == 0 then 100.0 else 100 * fromIntegral wins / fromIntegral total

-- |
avgUptimesForRows :: [PlayerStats] -> Uptimes
avgUptimesForRows rows = Uptimes (up feudal_time) (up castle_time) (up imperial_time)
    where up f = avgTime' $ f <$> rows

-- |
gameStats :: Game -> [PlayerStats]
gameStats = fmap thd3 . gamePlayerStats

-- |
avgTime' :: [Int] -> Maybe Time
avgTime' is = case filter (>0) is of
    []  -> Nothing
    is' -> mkTime . floor $ average is'

-- |
avgTime :: [Time] -> Time
avgTime ts = mkTime' $ floor (average $ unMkTime <$> ts)
