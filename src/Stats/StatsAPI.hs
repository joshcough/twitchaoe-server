
module Stats.StatsAPI (
    UnprotectedStatsAPI
  , unprotectedStatsServer
  ) where

import           Control.Monad               (forM, forM_, join, void)
import           Control.Monad.Except        (MonadIO)
import           Data.List.NonEmpty          (NonEmpty(..))
import           Data.Aeson                  ((.=))
import           Data.Maybe                  (catMaybes)
import           Data.Text                   (Text)
import           Servant.HTML.Blaze          (HTML)
import           Servant.Multipart           (FileData(..), MultipartData(..), MultipartForm, MultipartResult, Tmp)
import           Text.Blaze.Html5            (Html)

import           Config                      (AppT)
import           Logging                     (logDebug)

import           ServantHelpers
import           Stats.AoeFileReader         (processZipFile, readGameFile, readSplitGameFiles)
import           Stats.Models                (Game(..), GameImportResult(..), civByName)
import           Stats.Html                  ( AggregatedCivStatsResponse(..)
                                             , AggregatedMapStatsResponse(..)
                                             , AggregatedPlayerStatsResponse(..)
                                             , AggregatedTeamStatsResponse(..)
                                             , index)
import           Stats.Aggregations          (getStatsByPlayer, getStatsByTeam, getStatsByCiv, getStatsByMap,
                                              getStatsForCivs, getStatsForMaps, getStatsForPlayers, getStatsForTeams)
import           Stats.Storage               (insertResult, getPlayer, getMap, getTeam, getAllGames)

-- |
type ProtectedStatsAPI = "stats" :> Compose ProtectedStats

-- |
data ProtectedStats route = ProtectedStats {
    protectedStatsIndex :: route :- Get '[HTML] Html
  , protectedStatsUpload :: route :- "upload" :> Compose StatsUpload
} deriving Generic

-- |
data StatsUpload route = StatsUpload {
    statsUploadGameFiles :: route :- MultipartForm Tmp (MultipartData Tmp) :> Post '[JSON] [GameImportResult]
  , statsUploadZipFiles :: route :- "zip" :> MultipartForm Tmp (MultipartData Tmp) :> Post '[JSON] [(FilePath, Maybe GameImportResult)]
  , statsUploadSplitGames :: route :- "split" :> MultipartForm Tmp (MultipartData Tmp) :> Post '[JSON] (Maybe GameImportResult)
} deriving Generic

-- |
type UnprotectedStatsAPI = "stats" :> Compose UnprotectedStats

-- |
data UnprotectedStats route = UnprotectedStats {
    unprotectedStatsPlayers :: route :- "players" :> Compose StatsPlayers
  , unprotectedStatsTeams :: route :- "teams" :> Compose StatsTeams
  , unprotectedStatsCivs :: route :- "teams" :> Compose StatsCivs
  , unprotectedStatsMaps :: route :- "maps" :> Compose StatsMaps
  , unprotectedStatsGames :: route :- "games" :> Get '[JSON, HTML] [Game]
  , unprotectedStatsHidden :: route :- "hidden" :> Compose ProtectedStats
} deriving Generic

-- |
data StatsPlayers route = StatsPlayers {
    _statsPlayersAll :: route :- Get '[JSON, HTML] AggregatedPlayerStatsResponse
  , _statsPlayersIndividual :: route :- Capture "playerName" Text :> Get '[JSON, HTML] AggregatedPlayerStatsResponse
} deriving Generic

-- |
data StatsTeams route = StatsTeams {
    _statsTeamsAll :: route :- Get '[JSON, HTML] AggregatedTeamStatsResponse
  , _statsTeamsIndividual :: route :- Capture "teamName" Text :> Get '[JSON, HTML] AggregatedTeamStatsResponse
} deriving Generic

-- |
data StatsCivs route = StatsCivs {
    _statsCivsAll :: route :- Get '[JSON, HTML] AggregatedCivStatsResponse
  , _statsCivsIndividual :: route :- Capture "civName" Text :> Get '[JSON, HTML] AggregatedCivStatsResponse
} deriving Generic

-- |
data StatsMaps route = StatsMaps {
    _statsMapsAll :: route :- Get '[JSON, HTML] AggregatedMapStatsResponse
  , _statsMapsIndividual :: route :- Capture "mapName" Text :> Get '[JSON, HTML] AggregatedMapStatsResponse
} deriving Generic

-- |
protectedStatsServer :: MonadIO m => ServerT ProtectedStatsAPI (AppT m)
protectedStatsServer = toServant $ ProtectedStats {..}
    where
    protectedStatsIndex = return index
    protectedStatsUpload = toServant StatsUpload {
        statsUploadGameFiles = uploadRecording
      , statsUploadZipFiles = uploadZip
      , statsUploadSplitGames = uploadSplitGame
    }

-- |
unprotectedStatsServer :: MonadIO m => ServerT UnprotectedStatsAPI (AppT m)
unprotectedStatsServer = toServant $ UnprotectedStats {..}
    where
    unprotectedStatsPlayers = toServant $ StatsPlayers getStatsForPlayers' getStatsByPlayer'
    unprotectedStatsTeams = toServant $ StatsTeams getStatsForTeams' getStatsByTeam'
    unprotectedStatsCivs = toServant $ StatsCivs getStatsForCivs' getStatsByCiv'
    unprotectedStatsMaps = toServant $ StatsMaps getStatsForMaps' getStatsByMap'
    unprotectedStatsGames = getAllGames
    unprotectedStatsHidden = protectedStatsServer

-- |
uploadRecording :: MonadIO m => MultipartData Tmp -> AppT m [GameImportResult]
uploadRecording multipartData = do
    $(logDebug) "recording" []
    results <- catMaybes <$> forM (files multipartData) (processUploadedFile readGameFile)
    $(logDebug) "uploaded recording" ["results" .= results]
    forM_ results insertResult
    return results

-- |
uploadZip :: MonadIO m => MultipartData Tmp -> AppT m [(FilePath, Maybe GameImportResult)]
uploadZip multipartData = do
    $(logDebug) "uploadZip" []
    results <- join <$> forM (files multipartData) (processUploadedFile processZipFile)
    forM_ results $ \case
        (f, Nothing) -> $(logDebug) "could not read stats" ["game file" .= f]
        (_, Just r)  -> void $ insertResult r
    return results

-- |
uploadSplitGame :: MonadIO m => MultipartData Tmp -> AppT m (Maybe GameImportResult)
uploadSplitGame multipartData = do
    $(logDebug) "uploadSplitGame" []
    results <- case files multipartData of
        (x1:x2:xs) -> readSplitGameFiles (fdFileName x1) (fdPayload x1) (fdPayload <$> x2:|xs)
        []         -> error "no games uploaded" -- TODO throw a real error. see RER for example hopefully
        [_]        -> error "only one game uploaded"
    $(logDebug) "uploadedSplit Game" ["results" .= results]
    forM_ results insertResult
    return results

-- |
processUploadedFile :: (Text -> MultipartResult tag -> t) -> FileData tag -> t
processUploadedFile f fd = f (fdFileName fd) (fdPayload fd)

-- |
getStatsByCiv' :: MonadIO m => Text -> AppT m AggregatedCivStatsResponse
getStatsByCiv' civName =
    maybeOr404 (civByName civName) $ fmap (AggregatedCivStatsResponse . (: [])) . getStatsByCiv

-- |
getStatsForCivs' :: MonadIO m => AppT m AggregatedCivStatsResponse
getStatsForCivs' = AggregatedCivStatsResponse <$> getStatsForCivs

-- |
getStatsByPlayer' :: MonadIO m => Text -> AppT m AggregatedPlayerStatsResponse
getStatsByPlayer' playerName = do
    p <- getPlayer playerName >>= traverse getStatsByPlayer
    maybeOr404 p $ \p' -> return $ AggregatedPlayerStatsResponse [p']

-- |
getStatsForPlayers' :: MonadIO m => AppT m AggregatedPlayerStatsResponse
getStatsForPlayers' = AggregatedPlayerStatsResponse <$> getStatsForPlayers

-- |
getStatsByTeam' :: MonadIO m => Text -> AppT m AggregatedTeamStatsResponse
getStatsByTeam' playerName = do
    p <- getTeam playerName >>= traverse getStatsByTeam
    maybeOr404 p $ \p' -> return $ AggregatedTeamStatsResponse [p']

-- |
getStatsForTeams' :: MonadIO m => AppT m AggregatedTeamStatsResponse
getStatsForTeams' = AggregatedTeamStatsResponse <$> getStatsForTeams

-- |
getStatsByMap' :: MonadIO m => Text -> AppT m AggregatedMapStatsResponse
getStatsByMap' mapName = do
    p <- getMap mapName >>= traverse getStatsByMap
    maybeOr404 p $ \p' -> return $ AggregatedMapStatsResponse [p']

-- |
getStatsForMaps' :: MonadIO m => AppT m AggregatedMapStatsResponse
getStatsForMaps' = AggregatedMapStatsResponse <$> getStatsForMaps
