
module Auth.UserStorage (
    UserDb(..)
  ) where

import           Control.Arrow               ((&&&))
import           Control.Monad.Except        (MonadIO, liftIO)
import           Crypto.BCrypt               (hashPasswordUsingPolicy, slowerBcryptHashingPolicy)
import           Data.Bifunctor              (bimap)
import           Data.ByteString             (ByteString)
import           Data.Maybe                  (fromJust, listToMaybe)
import           Data.Text                   (Text)
import           Data.Text.Encoding          (decodeUtf8, encodeUtf8)
import           Data.UUID                   (fromText, toText)
import           Database.Esqueleto
import qualified Database.Persist.Postgresql as P
import           Data.UUID.V4                (nextRandom)

import           Auth.DatabaseModels         (DbTeamId, DbUser(..), DbTeam(..), EntityField(..), DbUserId)
import qualified Auth.DatabaseModels        as Db
import           Auth.Models                 (CreateUser(..), User(..), Team(..), TokenLogin(..))
import           Config                      (AppT', runDb)

class Monad m => UserDb m where
    getUserById :: DbUserId -> m (Maybe User)
    getUserByUsername :: Text -> m (Maybe User)
    getUserByEmail :: Text -> m (Maybe (User, Text))
    getUserAndTeamByUserEmail :: Text -> m (Maybe ((User, Text), Team))
    getUserAndTeamByUserToken ::TokenLogin -> m (Maybe (User, Team))
    getUsersByTeam :: DbTeamId -> m [User]
    deleteUserById :: DbUserId -> m ()
    createUser :: CreateUser -> m (Maybe DbUserId)
    -- TODO: this one is terrible.
    updateUserIfExists :: DbUserId -> User -> m ()

instance MonadIO m => UserDb (AppT' e m) where
    getUserById = runDb . getUserById
    getUserByUsername = runDb . getUserByUsername
    getUserByEmail = runDb . getUserByEmail
    getUserAndTeamByUserEmail = runDb . getUserAndTeamByUserEmail
    getUsersByTeam = runDb . getUsersByTeam
    getUserAndTeamByUserToken = runDb . getUserAndTeamByUserToken
    deleteUserById = runDb . deleteUserById
    createUser = runDb . createUser
    updateUserIfExists uid = runDb . updateUserIfExists uid

instance MonadIO m => UserDb (SqlPersistT m) where
    getUserById = fmap (fmap entityToUser) . getEntity

    getUserByUsername username = fmap entityToUser <$> selectFirst [Db.DbUserName P.==. username] []

    getUserByEmail email = fmap f <$> selectFirst [Db.DbUserEmail P.==. email] []
        where f e@(Entity _ dbUser) = (entityToUser e, dbUserHashedPassword dbUser)

    getUserAndTeamByUserEmail email = fmap (fmap f) $ getUserWhere $ \user _ -> (user ^. DbUserEmail) ==. val email
        where f = bimap (entityToUser &&& dbUserHashedPassword . entityVal) entityToTeam

    getUsersByTeam teamId = fmap (fmap entityToUser) . select $ from $ \user -> do
        where_ $ user ^. DbUserTeamId ==. val teamId
        return user

    getUserAndTeamByUserToken (TokenLogin t) =
        fmap (fmap $ bimap entityToUser entityToTeam) $
            getUserWhere $ \user _ -> (user ^. DbUserToken) ==. val (toText t)

    deleteUserById = P.deleteCascade

    createUser (CreateUser name teamId email pass settings) =
        liftIO (encryptPassword pass) >>= \case
            Nothing -> return Nothing
            Just password' -> do
               token <- liftIO nextRandom
               newUser <- insert $ DbUser (toSqlKey teamId) name email (decodeUtf8 password') settings False (toText token)
               return . Just $ newUser
        where
        encryptPassword :: Text -> IO (Maybe ByteString)
        encryptPassword = hashPasswordUsingPolicy slowerBcryptHashingPolicy . encodeUtf8

    updateUserIfExists uid (User _ teamId name email settings _ token) =
        getEntity uid >>= \case
            Just (Entity k v) -> replace k $
                DbUser (toSqlKey teamId) name email (dbUserHashedPassword v) settings (dbUserAdmin v) (toText token)
            Nothing -> return ()

-- |
getUserWhere :: (MonadIO m) => (SqlExpr (Entity DbUser) -> SqlExpr (Entity DbTeam) -> SqlExpr (Value Bool))
                            -> SqlPersistT m (Maybe (Entity DbUser, Entity DbTeam))
getUserWhere w = fmap listToMaybe . select . from $ \(user, team) -> do
    where_ (
            w user team
        &&. (team ^. DbTeamId) ==. (user ^. DbUserTeamId)
     )
    limit 1
    return (user, team)

-- |
entityToUser :: Entity DbUser -> User
entityToUser (Entity k DbUser {..}) =
    User (fromSqlKey k) (fromSqlKey dbUserTeamId) dbUserName dbUserEmail dbUserSettings dbUserAdmin (fromJust $ fromText dbUserToken)

-- | TODO: make a TeamStorage module, put this in there, and import from here.
entityToTeam :: Entity DbTeam -> Team
entityToTeam (Entity k v) = Team (fromSqlKey k) (dbTeamName v) (dbTeamSettings v)
