{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric  #-}

module Auth.Models (
    CreateTeam(..)
  , CreateUser(..)
  , Login(..)
  , LoginSuccess(..)
  , Team(..)
  , Token(..)
  , TokenLogin(..)
  , User(..)
  , UserToken(..)
  ) where

import           Data.Aeson             (FromJSON, ToJSON, Value)
import           Data.Int               (Int64)
import           Data.Text              (Text)
import           Data.UUID              (UUID)
import           GHC.Generics           (Generic)
import           Servant.Auth.Server
import           Web.HttpApiData        (FromHttpApiData(..))

import           Util.Utils             (HasJsonValue(..))

data Login = Login {
    loginEmail    :: Text
  , loginPassword :: Text
} deriving (Eq, Show, Generic, ToJSON, FromJSON)

data LoginSuccess = LoginSuccess {
    loginSuccessUser :: User
  , loginSuccessTeam :: Team
} deriving (Eq, Show, Generic, ToJSON, FromJSON)

newtype TokenLogin = TokenLogin {
    tokenLoginToken :: UUID
} deriving (Eq, Show, Read, Generic, ToJSON, FromJSON)

data Team = Team {
    teamId       :: Int64
  , teamName     :: Text
  , teamSettings :: Value
} deriving (Eq, Generic, Show, ToJSON, FromJSON)

instance HasJsonValue Team where
    getJson = teamSettings

data CreateTeam = CreateTeam {
    createTeamName     :: Text
  , createTeamSettings :: Value
} deriving (Eq, Generic, Show, ToJSON, FromJSON)

data User = User {
    userId       :: Int64
  , userTeamId   :: Int64
  , userName     :: Text
  , userEmail    :: Text
  , userSettings :: Value
  , userIsAdmin  :: Bool
  , userToken    :: UUID
} deriving (Eq, Generic, Show, ToJSON, FromJSON, ToJWT, FromJWT)

instance HasJsonValue User where
    getJson = userSettings

data CreateUser = CreateUser {
    createUserName     :: Text
  , createUserTeamId   :: Int64
  , createUserEmail    :: Text
  , createUserPassword :: Text
  , createUserSettings :: Value
} deriving (Eq, Generic, Show, ToJSON, FromJSON)

newtype Token = Token { getToken :: Text }
    deriving (Show, Eq, Generic, ToJSON, FromJSON)

newtype UserToken = UserToken { userTokenToken :: Token }
    deriving (Show, Eq, Generic, ToJSON, FromJSON)

instance FromHttpApiData UserToken where
    parseUrlPiece = pure . UserToken . Token