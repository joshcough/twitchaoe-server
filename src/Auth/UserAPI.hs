
module Auth.UserAPI where

import           Control.Monad.Except        (MonadError, MonadIO, liftIO)
import           Control.Monad.Reader        (MonadReader, asks)
import           Crypto.BCrypt               (validatePassword)
import           Data.Aeson                  (Value, (.=))
import           Data.Text                   (Text)
import           Data.Text.Encoding          (encodeUtf8)
import           Database.Persist.Postgresql (fromSqlKey)
import           ServantHelpers
import           Servant.Auth.Server

import           Auth.DatabaseModels         (DbUserId)
import           Auth.Models                 (CreateUser(..), Login(..), LoginSuccess(..), Team(..), TokenLogin(..), User(..))
import qualified Auth.UserStorage    as Db
import           Error                       (ProverlaysError)
import           Config                      (AppT, Config(..))
import           Logging                     (logDebug)
import           Util.Utils                  (jsonPathV, setJsonPath')

type SetCookieHeader  = Header "Set-Cookie" SetCookie
type SetCookieHeaders = '[SetCookieHeader, SetCookieHeader]

---
--- Login API/Server
---

type LoginAPI = "login" :> Compose LoginServer

data LoginServer route = LoginServer {
    loginServerLogin :: route :-  ReqBody '[JSON] Login :> Post '[JSON] (Headers SetCookieHeaders LoginSuccess)
  , loginServerTokenLogin :: route :- "token" :> ReqBody '[JSON] TokenLogin :> Post '[JSON] (Headers SetCookieHeaders LoginSuccess)
  } deriving Generic

loginServer :: MonadIO m => ServerT LoginAPI (AppT m)
loginServer = toServant $ LoginServer login tokenLogin

{-
 - Here is the login handler. We do the following:
 - A) look up the user in the database by email addr, and throw 404 if not found
 - B) Check to see if they entered a valid password, and throw a 401 if not
 - C) Return the jwt token in the header.
 -}
login :: MonadIO m => Login -> AppT m (Headers SetCookieHeaders LoginSuccess)
login (Login e pw) = do
    maybeUT <- Db.getUserAndTeamByUserEmail e
    maybeOr401 maybeUT $ \((user, hashedPw), team) -> guard401 (validate hashedPw) (applyCookies user team)
    where
    validate hashedPw = validatePassword (encodeUtf8 hashedPw) (encodeUtf8 pw)

-- |
tokenLogin :: MonadIO m => TokenLogin -> AppT m (Headers SetCookieHeaders LoginSuccess)
tokenLogin t = Db.getUserAndTeamByUserToken t >>= flip maybeOr401 (uncurry applyCookies)

-- |
applyCookies :: (MonadError ProverlaysError m, MonadIO m, MonadReader Config m) =>
    User -> Team -> m (Headers SetCookieHeaders LoginSuccess)
applyCookies usr team = do
    cookieSettings <- asks _configCookies
    jwtSettings    <- asks _configJWT
    mApplyCookies  <- liftIO $ acceptLogin cookieSettings jwtSettings usr
    maybeOr401 mApplyCookies (\app -> return . app $ LoginSuccess usr team)

---
--- User API/Server
---

type UserAPI = "users" :> Compose UserServer

data UserServer route = UserServer {
    userServerGetUserById :: route :- Capture "id" DbUserId :> Get '[JSON] User
  , userServerDeleteUser :: route :- Capture "id" DbUserId :> Delete '[JSON] ()
  , userServerCreateUser :: route :- ReqBody '[JSON] CreateUser :> Post '[JSON] DbUserId
  , userServerUpdateUser :: route :- Capture "id" DbUserId :> ReqBody '[JSON] User :> Put '[JSON] ()
  , userServerSettings :: route :- Capture "id" DbUserId :> "settings" :> Compose UserSettingsAPI
  } deriving Generic

-- |
data UserSettingsAPI route = UserSettingsAPI {
    userSettingsGetPath :: route :- Capture "path" Text :> Get '[JSON] (Maybe Value)
  , userSettingsUpdatePath :: route :- Capture "path" Text :> ReqBody '[JSON] Value :> Post '[JSON] ()
} deriving Generic

-- | The server that runs the UserAPI
userServer :: (MonadIO m) => User -> ServerT UserAPI (AppT m)
userServer caller = toServant $ UserServer { .. }
    where
    userServerGetUserById = getUserById caller
    userServerDeleteUser = deleteUser caller
    userServerCreateUser = createUser caller
    userServerUpdateUser = updateUser caller
    userServerSettings uid = toServant UserSettingsAPI {
      userSettingsGetPath = getUserSettingsPath caller uid
    , userSettingsUpdatePath = updateUserSettingsPath caller uid
    }

-- | Returns a user by name or throws a 404 error.
getUserById :: MonadIO m => User -> DbUserId -> AppT m User
getUserById caller uid = do
    $(logDebug) "getUserById" ["uid" .= uid]
    callerIsUserOrIsAdminElse401 caller uid $ withUserOr404 uid return

-- | Returns a user by name or throws a 404 error.
-- TODO: remove this as soon as the websocket auth is fixed
getUserByIdUNSAFE :: MonadIO m =>  DbUserId -> AppT m User
getUserByIdUNSAFE uid = do
    $(logDebug) "getUserById" ["uid" .= uid]
    withUserOr404 uid return

-- | Creates a user in the database.
deleteUser :: MonadIO m => User -> DbUserId -> AppT m ()
deleteUser caller uid = do
    $(logDebug) "deleteUser" ["uid" .= uid]
    adminOr401 caller $ withUserOr404 uid (const $ Db.deleteUserById uid)

-- | Creates a user in the database.
createUser :: MonadIO m => User -> CreateUser -> AppT m DbUserId
createUser caller c = do
    $(logDebug) "createUser" []
    adminOr401 caller $ Db.createUser c >>= flip (maybeOr500 "Couldn't create user.") return

-- | Update a user in the database.
updateUser :: MonadIO m => User -> DbUserId -> User -> AppT m ()
updateUser caller uid u = do
    $(logDebug) "updateUser" ["uid" .= uid, "user" .= u]
    callerIsUserOrIsAdminElse401 caller uid $ withUserOr404 uid . const $ Db.updateUserIfExists uid u

-- | lookup a path in the user's settings json
getUserSettingsPath :: MonadIO m => User -> DbUserId -> Text -> AppT m (Maybe Value)
getUserSettingsPath caller uid path = jsonPathV path <$> getUserById caller uid

-- | update a path in the user's settings json
updateUserSettingsPath :: MonadIO m => User -> DbUserId -> Text -> Value -> AppT m ()
updateUserSettingsPath caller uid path newValue = do
    User {..} <- getUserById caller uid
    let newSettings = setJsonPath' path newValue userSettings
    updateUser caller uid $ User (fromSqlKey uid) userTeamId userName userEmail newSettings userIsAdmin userToken

-- | Look up a user by id. If it exist, run an operation on it. If not, throw a 404.
withUserOr404 :: MonadIO m => DbUserId -> (User -> AppT m b) -> AppT m b
withUserOr404 uid m = Db.getUserById uid >>= flip maybeOr404 m
