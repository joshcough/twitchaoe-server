
module Auth.TeamAPI where

import           Control.Monad.Except        (MonadIO)
import           Data.Aeson                  (Value, (.=))
import           Data.Text                   (Text)
import           Database.Persist.Postgresql (fromSqlKey)

import           Auth.DatabaseModels         (DbTeamId)
import           Auth.Models                 (CreateTeam(..), Team(..), User(..))
import qualified Auth.TeamStorage    as TeamsDb
import qualified Auth.UserStorage    as UsersDb
import           Config                      (AppT)
import           Logging                     (logDebug)
import           ServantHelpers
import           Util.Utils                  (jsonPathV, setJsonPath')

type TeamAPI = "teams" :> Compose TeamServer

data TeamServer route = TeamServer {
    teamServerGetTeamById :: route :- Capture "id" DbTeamId :> Get '[JSON] Team
  , teamServerDeleteTeam :: route :- Capture "id" DbTeamId :> Delete '[JSON] ()
  , teamServerCreateTeam :: route :- ReqBody '[JSON] CreateTeam :> Post '[JSON] DbTeamId
  , teamServerUpdateTeam :: route :- Capture "id" DbTeamId :> ReqBody '[JSON] Team :> Put '[JSON] ()
  , teamServerSettings :: route :- Capture "id" DbTeamId :> "settings" :> Compose TeamSettingsAPI
  , teamServerListTeams :: route :- "list" :> Get '[JSON] [Team]
  , teamServerListUsers :: route :- "users" :> Capture "id" DbTeamId :> Get '[JSON] [User]
  } deriving Generic

-- |
data TeamSettingsAPI route = TeamSettingsAPI {
    teamSettingsGetPath :: route :- Capture "path" Text :> Get '[JSON] (Maybe Value)
  , teamSettingsUpdatePath :: route :- Capture "path" Text :> ReqBody '[JSON] Value :> Post '[JSON] ()
} deriving Generic

-- | The server that runs the TeamAPI
teamServer :: (MonadIO m) => User -> ServerT TeamAPI (AppT m)
teamServer caller = toServant $ TeamServer { .. }
    where
    teamServerGetTeamById = getTeamById caller
    teamServerDeleteTeam = deleteTeam caller
    teamServerCreateTeam = createTeam caller
    teamServerUpdateTeam = updateTeam caller
    teamServerSettings tid = toServant TeamSettingsAPI {
      teamSettingsGetPath = getTeamSettingsPath caller tid
    , teamSettingsUpdatePath = updateTeamSettingsPath caller tid
    }
    teamServerListTeams = getTeams
    teamServerListUsers = UsersDb.getUsersByTeam

-- | Returns a team by name or throws a 404 error.
getTeamById :: MonadIO m => User -> DbTeamId -> AppT m Team
getTeamById caller tid = do
    $(logDebug) "getTeamById" ["caller" .= caller, "teamId" .= tid]
    maybeTeam <- TeamsDb.getTeamById tid
    maybeOr404 maybeTeam (callerIsOnTeamOrIsAdminElse401 caller tid . return)

-- | Creates a team in the database.
deleteTeam :: MonadIO m => User -> DbTeamId -> AppT m ()
deleteTeam caller tid = do
    $(logDebug) "deleteTeam" ["caller" .= caller, "teamId" .= tid]
    adminOr401 caller $ do
        maybeTeam <- TeamsDb.getTeamById tid
        maybeOr404 maybeTeam . const $ TeamsDb.deleteTeamById tid

-- | Creates a team in the database.
createTeam :: MonadIO m => User -> CreateTeam -> AppT m DbTeamId
createTeam caller c = do
    $(logDebug) "createTeam" ["caller" .= caller, "CreateTeam" .= c]
    adminOr401 caller $ TeamsDb.createTeam c

-- | Update a team in the database.
-- TODO: we probably want to add some roles, as most users won't be able to update the team. only team admins.
updateTeam :: MonadIO m => User -> DbTeamId -> Team -> AppT m ()
updateTeam caller tid t = do
    $(logDebug) "updateTeam" ["caller" .= caller, "teamId" .= tid, "team" .= t]
    callerIsOnTeamOrIsAdminElse401 caller tid $ do
        maybeTeam <- TeamsDb.getTeamById tid
        maybeOr404 maybeTeam . const $ TeamsDb.updateTeamIfExists tid t

-- | lookup a path in the team's settings json
getTeamSettingsPath :: MonadIO m => User -> DbTeamId -> Text -> AppT m (Maybe Value)
getTeamSettingsPath caller tid path = jsonPathV path <$> getTeamById caller tid

-- | update a path in the team's settings json
updateTeamSettingsPath :: MonadIO m => User -> DbTeamId -> Text -> Value -> AppT m ()
updateTeamSettingsPath caller tid path newValue = do
    Team {..} <- getTeamById caller tid
    let newSettings = setJsonPath' path newValue teamSettings
    TeamsDb.updateTeamIfExists tid $ Team (fromSqlKey tid) teamName newSettings

-- | Get all the teams from the db.
getTeams :: MonadIO m => AppT m [Team]
getTeams = TeamsDb.getTeams

-- | Get all the users on the team.
-- TODO: this should probably be scoped to an admin role or something...
getUsersByTeam :: MonadIO m => User -> DbTeamId -> AppT m [User]
getUsersByTeam caller tid = do
    $(logDebug) "getUsersByTeam" ["caller" .= caller, "teamId" .= tid]
    callerIsOnTeamOrIsAdminElse401 caller tid $ UsersDb.getUsersByTeam tid
