
module Auth.TeamStorage (
    UserDb(..)
  ) where

import           Control.Monad.Except        (MonadIO)
import           Data.Text                   (Text)
import           Database.Esqueleto
import qualified Database.Persist.Postgresql as P

import           Auth.DatabaseModels         (DbTeamId, DbTeam(..))
import qualified Auth.DatabaseModels        as Db
import           Auth.Models                 (CreateTeam(..), Team(..))
import           Config                      (AppT', runDb)

class Monad m => UserDb m where
    createTeam :: CreateTeam -> m DbTeamId
    getTeamById :: DbTeamId -> m (Maybe Team)
    getTeamByTeamname :: Text -> m (Maybe Team)
    deleteTeamById :: DbTeamId -> m ()
    updateTeamIfExists :: DbTeamId -> Team -> m ()
    getTeams :: m [Team]

instance MonadIO m => UserDb (AppT' e m) where
    createTeam = runDb . createTeam
    getTeamById = runDb . getTeamById
    getTeamByTeamname = runDb . getTeamByTeamname
    updateTeamIfExists tid = runDb . updateTeamIfExists tid
    deleteTeamById = runDb . deleteTeamById
    getTeams = runDb getTeams

instance MonadIO m => UserDb (SqlPersistT m) where
    createTeam (CreateTeam name settings) = insert $ DbTeam name settings

    getTeamById = entityToTeam . getEntity

    getTeamByTeamname teamname = entityToTeam $ P.selectFirst [Db.DbTeamName P.==. teamname] []

    updateTeamIfExists tid (Team _ name settings) = getTeamById tid >>= \case
        Just (Team k _ _) -> replace (toSqlKey k) $ DbTeam name settings
        Nothing -> return ()

    deleteTeamById = P.deleteCascade

    getTeams = entityToTeam $ P.selectList [] []

---
--- Helpers
---

entityToTeam :: (Functor f, Functor g) => f (g (Entity DbTeam)) -> f (g Team)
entityToTeam = fmap (fmap f)
    where f (Entity k v) = Team (fromSqlKey k) (dbTeamName v) (dbTeamSettings v)
