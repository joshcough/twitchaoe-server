{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Auth.DatabaseModels where

import           Data.Aeson                       (Value)
import           Database.Persist.Postgresql.JSON ()
import           Database.Persist.TH              (mkDeleteCascade, mkPersist, persistLowerCase, share, sqlSettings)
import           Data.Text                        (Text)

share [mkPersist sqlSettings, mkDeleteCascade sqlSettings] [persistLowerCase|
DbTeam json sql=teams
    name             Text
    settings         Value
    DbTeamUniqueName name
    deriving Show Eq

DbUser json sql=users
    teamId         DbTeamId
    name           Text
    email          Text
    hashedPassword Text
    settings       Value
    admin          Bool
    token          Text
    DbUserUniqueEmail email
    DbUserLogin email hashedPassword
    DbUserUniqueToken token
    deriving Show Eq
|]
