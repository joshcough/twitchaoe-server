
module Util.Utils (
    average
  , getRecursiveContents
  , jsonPath
  , jsonPath'
  , jsonPathV
  , setJsonPath
  , setJsonPath'
  , HasJsonValue(..)
  , mapFromList'
  , strength
  , (^?)
  , tShow
) where

import           Control.Lens                   (Traversal', alaf, (^?))
import           Control.Monad                  (forM)
import           Control.Monad.Except           (MonadIO, liftIO)
import           Data.Aeson                     (Value(..))
import           Data.Aeson.Lens                (key)
import qualified Data.HashMap.Strict            as HM
import           Data.List                      (genericLength, foldl')
import           Data.Map                       (Map)
import qualified Data.Map                       as Map
import           Data.Maybe                     (fromMaybe)
import           Data.Monoid
import           Data.Text                      (Text, pack, split)
import           System.Directory               (doesDirectoryExist, getDirectoryContents)
import           System.FilePath                ((</>))

average :: Real a => [a] -> Double
average xs = realToFrac (sum xs) / genericLength xs

-- From Real World Haskell, p. 214
getRecursiveContents :: MonadIO m => FilePath -> m [FilePath]
getRecursiveContents topPath = liftIO $ do
  names <- getDirectoryContents topPath
  let properNames = filter (`notElem` [".", ".."]) names
  paths <- forM properNames $ \name -> do
    let path = topPath </> name
    isDirectory <- doesDirectoryExist path
    if isDirectory
      then getRecursiveContents path
      else return [path]
  return (concat paths)

jsonPath :: [Text] -> Traversal' Value Value
jsonPath = alaf Endo foldMap key

jsonPath' :: Text -> Traversal' Value Value
jsonPath' = jsonPath . split (== '.')

jsonPathV :: HasJsonValue a => Text -> a -> Maybe Value
jsonPathV path a = getJson a ^? jsonPath' path

-- setJsonPath :: Text -> Value -> Value -> Value
-- setJsonPath path newVal = set (jsonPath' path) newVal

setJsonPath' :: Text -> Value -> Value -> Value
setJsonPath' path = setJsonPath $ split (== '.') path

setJsonPath :: [Text] -> Value -> Value -> Value
setJsonPath path newVal val = case path of
    [] -> val
    [p] -> case val of
        (Object o) -> Object $ HM.insert p newVal o
        _ -> Object $ HM.insert p newVal HM.empty
    (p:ps) -> case val of
        (Object o) -> Object $ HM.insert p (setJsonPath ps newVal val') o
            where val' = fromMaybe (Object HM.empty) $ HM.lookup p o
        _ -> Object $ HM.insert p (setJsonPath ps newVal (Object HM.empty)) HM.empty

class HasJsonValue a where
    getJson :: a -> Value

-- | Create a Map from a list, but if there are duplicate keys, collect them into a list.
mapFromList' :: Ord k => [(k, a)] -> Map k [a]
mapFromList' = foldl' insert' Map.empty
    where
    insert' m (k, a) = Map.alter f k m
        where f Nothing   = Just [a]
              f (Just as) = Just (a:as)

-- | For some theory about this function, see https://bartoszmilewski.com/2017/02/06/applicative-functors/
strength :: Functor f => (a, f b) -> f (a, b)
strength (a, fb) = (a,) <$> fb

-- |
tShow :: Show a => a -> Text
tShow = pack . show